# Teoria dos Grafos
(Faltam as imagens, um docente DInf e formatação própria [e autoria, que fica por último])

por Marcus Vinícius Reisdoefer Pereira
## 1. Visão geral

* No desenvolver da Ciência da Computação percebeu-se que o estudo de tópicos relacionados à Matemática Discreta tornaram-se não apenas interessantes, mas essenciais para uma maior compreensão dos conteúdos da computação. Conteúdos como **lógica proposicional**, **teoria de conjuntos**, **relações**, **funções**, **probabilidade discreta**, etc., são exemplos da utilidade do estudo dessas duas áreas em união, afinal, já se tornou conhecimento comum que cursos voltados à área da computação requerem um forte entendimento de diversas áreas da Matemática.
* O estudo de **Teoria dos Grafos** voltado especificamente para a Ciência da Computação não se mostra diferente dos exemplos anteriores, e é fundamental para que vários conceitos mais avançados, principalmente de **Estruturas de Dados** e **Algoritmos**, sejam compreendidos completamente, justamente pelo conhecimento base que o estudo sobre grafos fornece; portanto, é imprescindível construir essa noção matemática e entender como aplicá-la na computação em si.

    ------(imagens exemplares de grafos [3 ou 4 deve dar])-----

    ### 1.1. O que é um grafo?
    
    A maneira mais simples de compreender o que é um grafo [^1] é vê-los como redes que conectam vários valores/componentes por meio de relações, de uma maneira simples de visualizar, isto é, para grafos menores.
    Nesse tipo de representação, os **vértices** representam os valores a serem estudados, e comumente são desenhados como círculos grandes o suficiente para facilitar a visualização. Da mesma maneira, as **arestas**, que são as relações conectando esses vértices, são as linhas retas representadas no grafo abaixo.
    
    [^1]: O nome dos grafos se deve ao fato que eles possuem uma representação gráfica que auxilia a compreensão de suas propriedades. Entretanto, é fato que não há apenas uma maneira de representar grafos graficamente. Por exemplo, veja a "imagem exemplar 2" que mostra **o mesmo grafo** desenhado de duas maneiras distintas.
    
   ------(imagem exemplar 2)------
   
    ### 1.2. Definição formal
    * Assim como qualquer coisa da Matemática, é necessário formalizar esse conceito utilizando uma linguagem universal para que quem tem o objetivo de estudar e avançar nessa área. Para isso, precisa-se compreender a estrutura dos grafos.
     
        #### 1.2.1. Componentes de um grafo:
        Formalmente, define-se um grafo **`G`** como sendo um **par ordenado**, em que o primeiro valor é o conjunto contendo os vértices `V(G)` e o segundo é o conjunto de suas arestas `E(G)`[^2]("E" vem de *edges*, do inglês), que conectam os diversos componentes desse grafo. Mais especificamente, uma aresta é definida como um conjunto de vértices, um **par não-ordenado** de vértices, de modo que uma aresta sempre estará relacionada a dois vértices, normalmente distintos.

        ------(grafo G para demonstração da linguagem abaixo)------

        #### 1.2.2. Grafo definido como par ordenado:
        * Na linguagem matemática, o grafo G da imagem anteriormente mostrada seria definido como:
            #### **G = ( V(G), E(G) )**
        > Sendo `V = { v0, v1, v2, v3 }` e `E(G) = { {v1,v2}, {v3,v4}, {v1,v3} }`

    [^2]: **Importante**: Dentre as diversas maneiras de representação há algumas em que o grafo é descrito como um trio ordenado, de modo que as arestas só são representadas por seus nomes, e incluindo como terceiro termo uma função de mapeamento entre os vértices e as arestas. Para mais informações referencie "9.1. Teoria dos Grafos, Bondy Murty".
            
    ### 1.3. Relação com Ciência da Computação:
    * Por mais que a importância do estudo de grafos para a Ciência da Computação tenha sido mencionada, o fato é que em nenhum momento um exemplo prático foi dado ao leitor. Portanto, abaixo estão dois exemplos práticos para o leitor que ainda não estava certo da importância desse tópico.
    
        #### 1.3.1. Representação de estruturas de dados:
        Um conceito fundamental para qualquer estudante de Ciência de Computação são **estruturas de dados** e os algoritmos envolvidos com essas estruturas, que também têm origem, muitas vezes, na Teoria de Grafos. Acontece que muitas estruturas de dados nada mais são do que uma interpretação de uma forma específica de grafo, ou até mesmo vice-versa, nomeadas relativamente diferente e utilizadas amplamente sem se pensar nessa relação essencial.
        * Um perfeito exemplo de estrutura de dado é o conceito de **árvore binária**, que se popularizou devido à sua aparição frequente em entrevistas de emprego para empresas famosas nos últimos anos.
            ##### 1.3.1.1. Árvores binárias:
            Um tipo de grafo que é comumente mencionado na área da programação, especialmente em entrevistas de emprego de grandes empresas como *Google*, *Facebook*, *Netflix*, etc., é a *Árvore binária*. Grafos conectados e sem ciclos (referencie "2.Terminologia Básica") são chamados de **árvores**. Uma árvore é dita **binária** quando um único vértice liga-se, no máximo, a outros dois, chamados de filhos esquerdo e direito, formando uma estrutura visual semelhante à de uma árvore invertida.
    
            ------(binary trees)------
            
            O mesmo se aplica a diversas outras estruturas de dados. Para fazer essa relação basta compreender incialmente a própria estrutura de dado ou vários conceitos de grafos, então é totalmente possível "traduzir" esses conceitos de uma linguagem para a outra.
            
        > Um bom exercício para compreender essa relação é traduzir estruturas de dados para a terminologia de teoria dos grafos, e vice-versa, assim compreender a relação entre os dois tópicos será simples.
        
        #### 1.3.2. Algoritmo do labirinto:
        Outra maneira que teoria dos grafos pode se comunicar com a Ciência da Computação é através de algoritmos relacionados aos grafos. Um bom exemplo de algoritmo de grafo é o algoritmo de busca, que pode ser aplicado extensivamente em diversos casos.
        Um caso específico que será citado é a **busca pela saída de um labirinto**. Normalmente, o ser humano simplesmente tentaria resolver o problema sem pensar exatamente no algoritmo envolvido com o próprio pensamento. Entretanto, é função de um programador formalizar esse algoritmo e resolver o problema com o uso do processamento de um computador.
        Por sorte, este desafio em específico naturalmente se traduz para a Teoria de Grafos, pois para transformar o labirinto em um grafo basta interpretar cada corredor como sendo uma aresta e os vértices como sendo intersecções do labirinto. Intuitivamente, o grafo gerado terá forma similar à do labirinto.
        
        ------(labirinto e grafo no mesmo formato)------
        
        * Note que para facilitar a visualização, o labirinto não era demasiadamente complexo, mas seria justamente para labirintos mais complexos que a solução por algoritmos computacionais realmente traria uma grande ajuda.
        
        > Lembre-se que grafos são descritos como um conjunto de dados relacionados para um computador, e que as ilustrações que vemos são meramente para facilitar a compreensão humana.
        
        ------(busca da saída do labirinto visualmente e com um grafo [se der tempo... deve dar, tá quase lá])------

## 2. Terminologia Básica

* Inicialmente, para compreender melhor a Teoria dos Grafos, é necessário ter uma noção básica de toda a terminologia envolvida, pois isso facilita a compreensão de conceitos mais complexos que derivam dessas definições básicas. Inicialmente a quantidade de definições pode parecer sobrecarregar quem recém iniciou os estudos, mas a paciência é essencial para aprender o que esses nomes representam.

    > **Uma dica é:** Assim como qualquer outra área da Ciência da Computação, o ideal é construir o conhecimento de maneira a sempre desenvolver as ideias originais, por isso a compreensão dos principais termos abaixo é de suma importância para um aprendizado proveitoso.

    ### 2.1. Arestas incidentes:
    Arestas são chamadas de incidentes para os vértices que conectam. Por exemplo, se uma aresta for definida como **e = {a,b}** diz-se que e é uma **aresta incidente em `a` e `b`**.
    
    ### 2.2. Vértices adjacentes:
    De mesma maneira, dois vértices serão adjacentes se e somente se existir alguma aresta conectando-os. Como um exemplo do grafo acima, `a` é adjacente a `b`, assim como `b` é adjacente a `a`.
    
    ### 2.3. Vértices isolados:
    Ainda, um vértice pode ser isolado, como o próprio nome implica, ele não faz conexões com nenhum outro vértice, isto é, não há aresta conectada a ele. Como exemplo disso no grafo acima, temos o vértice `e`.
    
    > Inclusive, um grafo sem aresta alguma é totalmente válido, e geralmente é descrito como **G = ( V(G),Ø )** para representar a ausência de arestas. Entretanto, esse grafo não seria realmente útil, afinal não demonstra qualquer relação e seria apenas uma coleção de dados soltos.
    
    ------(Grafo sem arestas)------
    
    ### 2.4. Avaliando o direcionamento de um grafo
    * Existem duas maneiras de representar conexões num grafo utilizando suas arestas, e esses modos serão construindo um grafo **direcionado** e um grafo **não direcionado**, ambos representados na figura abaixo.
        
        ------(grafo triangular direcionado e não direcionado lado a lado)------
        
        #### 2.4.1. Grafos não direcionados:
        São os grafos que foram apresentados até agora. Esse tipo de grafo, como o nome implica, **não** promove uma relação direcional entre seus componentes, ou seja, uma aresta que liga `v1` a `v2` é exatamente a mesma que liga `v2` a `v1`.
        
        #### 2.4.2. Grafos direcionados:
        Esse tipo de grafo é representado ligeiramente diferente do outro, pois as arestas passam a ser setas ao invés de linhas, isso devido ao fato de que as relações não necessariamente ocorrem reciprocamente. Ou seja, uma aresta não é mais representada por um conjunto de dois vértices, mas um **par ordenado de dois vértices** para dizer que esta aresta "sai do vértice `v1` e vai ao vértice `v2`", mas a aresta que "vai de `v2` a `v1`" não é a mesma, pois aponta na direção contrária.
    
        > **Aresta não direcionada:** `e = { a,b }` *(notação de conjunto)*
        
        > **Aresta direcionada:** `e = ( a,b )` *(notação de par ordenado)*
    
    ### 2.5. Grau de um vértice:
    * O grau de um vértice, `deg(v)` é o número de arestas do grafo que **incidem** em `v`. Note que se um vértice incidir em si mesmo o grau sobe 2 números, e não apenas 1, pois o vértice possui "duas saídas de si mesmo", que é a propriedade que define o grau.
        
        > **Dica:** Veja o grau de um vértice como o número de vezes em que ele aparece no conjunto de arestas do grafo. 
    
    * **V(G) = { ... }**
    
        ------(grafo com exemplo de laço)------
    
    * Perceba que a lógica é a mesma para um gráfico direcionado.
        
        ------(grafo direcionado)------
        
        #### 2.5.1. Grau máximo dos vértices de um grafo:
        Denota-se o grau máximo dos vértices de um grafo G, isto é, o maior grau de um vértice pertencendo a esse grafo, como a letra grega *delta* maiúsculo. Veja a imagem abaixo.
        > Δ = x
        
        ------(grafo aleatório)------
        
        #### 2.5.2. Grau mínimo dos vértices de um grafo:
        Denota-se o grau mínmo dos vértices de um grafo G, isto é, o menor grau de um vértice pertencendo a esse grafo, como a letra grega *delta* minúsculo. Veja a imagem acima.
        > δ = y
        
        - **Detalhe importante**: para grafos **direcionados** esses graus de vértices se subdividem em:
            - Grau máximo de **entrada**;
            - Grau mínimo de **entrada**;
            - Grau máximo de **saída**;
            - Grau mínimo de **saída**;
        
        ------(graus de saída e entrada de um grafo direcionado)------
        
        #### 2.5.3. Grafo regular:
        Utilizando a definição de grau de um vértice, pode-se dizer se um grafo é um grafo **regular** ou não. Em suma, um grafo só será regular se todos os seus vértices tiverem grau de valência iguais, isto é, cada vértice se conecta a um mesmo número de vértices.
        
        > **∀v ∈ V, deg(v) = n**
        
        Um grafo regular de `n` vértices é comumente chamado de **grafo n-regular**. Por exemplo, um grafo regular de 4 vértices será um **grafo 4-regular**.
        
        ------(grafo regular vs. grafo não-regular)------
        
    ### 2.6. Percorrendo um grafo:
    * Às vezes, para determinados algoritmos, é útil percorrer o grafo de uma maneira específica, e para isso também é necessária uma determinada terminologia a fim de simplificar a comunicação da intenção desses algoritmos.
    
        #### 2.6.1. Caminhos:
        * Um caminho em um grafo é uma sequência de vértices e arestas, com o objetivo de partir de um ponto inicial até um ponto final. Assim, se queremos dizer que há um caminho entre dois vértices **x** e **y**, dizemos que estamos falando de um **caminho-xy**.
        * Para descrevermos formalmente um caminho, listamos uma sequência alternada de vértices partindo do ponto inicial, o vértice **x**, e listanto os **n vértices** e as **n' arestas** até se chegar no vértice final **y**. 
        > (walk) w = **x** e1 v1 e2 v2 e3 **y**
        
        ------(gif caminho w acima)------
        
        * Os caminhos possuem inúmeras propriedades as quais podem lhes dar diferentes nomes para classificação.
            ##### 2.6.1.1. Comprimento do caminho:
            O comprimento de um caminho é medido através da contagem do número de arestas percorridas. No exemplo acima, o caminho **w** possui comprimento **3**.[^3]
            [^3]: A **distância entre dois vértices** é o comprimento do caminho mais curto possível entre esses dois vértices.
            
            ##### 2.6.1.2. Caminho fechado:
            Um caminho é dito fechado quando o vértice de partida **x** e o vértice de chegada **y** são iguais, isto é, o caminho possui deslocamento 0.
            Exemplo de caminho fechado com x = y:
            
            ------(gif caminho fechado)------
            
            ##### 2.6.1.3. Caminho simples:
            Um caminho simples é definido como um caminho que não possui vértices repetidos.
            
            ------(gif caminho simples)------
            
            ##### 2.6.1.4. Caminho euleriano:
            Um caminho euleriano é definido como um caminho que passa por **todas** as `arestas` exatamente uma vez, sem repetições (não confunda aresta com vértice).
            
            ------(gif caminho euleriano vs. caminho não euleriano)------
            
            Uma propriedade de grafos que possuem um caminho euleriano é que esse grafo terá, no máximo, dois vértices de grau ímpar.

            ##### 2.6.1.5. Caminho hamiltoniano:
            Um caminho hamiltoniano é definido como um caminho que passa por **todos** os `vértices` exatamente uma vez, sem repetições (não confunda vértice com aresta).
            
            ------(gif caminho hamiltoniano vs. caminho não hamiltoniano)------
            
            ##### 2.6.1.6. Ciclo:
            Um caminho é chamado de **ciclo** se ele for um caminho fechado **e** um caminho simples também, isto é, iniciar em um vértice e terminar nele mesmo, mas sem repetir outros vértices no processo de caminhada.
            
            ------(gif ciclo vs. caminho fechado não-cíclico)------
            
            * Ainda há tipos específicos de ciclos:
            
                ##### 2.6.1.6.1. Ciclos Simples:
                Um **ciclo simples** é um caminho simples que também é um ciclo, ou seja, essa terminologia é redundante, e pode ser omitida.
                
                ##### 2.6.1.6.2. Ciclos Eulerianos:
                Ciclos eulerianos são caminhos eulerianos, que têm início e final no mesmo ponto.
                
                ##### 2.6.1.6.3. Ciclos Hamiltonianos:
                Ciclos hamiltonianos são caminhos hamiltonianos, que têm início e final no mesmo ponto.
                
            #### 2.6.1.6. Caminhos com peso:
            As arestas de um grafo podem ter peso, isto é, uma aresta pode ter um valor maior do que outra. Isso pode ser importante quando se está lidando com a análise de tráfego em uma cidade, por exemplo, para poder definir a rota mais interessante para percorrer.
            
            - Uma nova propriedade de caminhos quando arestas possuem peso é o **peso do caminho**, que é basicamente definido como a soma dos pesos das arestas do caminho.
            
            ------(grafo com arestas com peso)------
            
        #### 2.6.2. Laços: 
        * Laços, ou *loops* do inglês, são arestas que conectam um vértice a si próprio.
        
        ### ATENÇÃO!
            As definições acima foram feitas com relação a **grafos não direcionados**. Para grafos direcionados, sempre veja "caminho" como "caminho direcionado" e "ciclo" como "ciclo direcionado".
        
    ### 2.7. Conectividade:
    * Uma propriedade importante para estudar em Teoria dos Grafos é a conectividade de algum elemento. Em específico, os mais importantes são:
        
        ##### 2.7.1. Conectividade de vértices:
        Dois vértices estão conectados somente se existir um caminho entre eles, isto é, um conjunto de arestas que, na sequência correta, iniciem uma caminhada em um e acabe no outro.
        
        ##### 2.7.2. Conectividade em grafos:
        *   Mais uma vez, esse conceito funciona ligeiramente diferente para grafos direcionados e grafos não direcionados.
        
            ###### 2.7.2.1. Conectividade de um grafo não direcionado:
            Um grafo não direcionado é dito **conexo** quando `todos os seus vértices estão conectados`.
            
            ###### 2.7.2.1. Conectividade de um grafo direcionado:
            Um grafo direcionado pode ser **fortemente conexo** ou **fracamente conexo**:
            - Grafo fortemente conexo:
                Define-se **fortemente conexo** o grafo direcionado que possui um caminho direcionado de todos os vértices para todos os vértices, isso é:
                > ∀`v,u` ∈ V, ∃`w`| `w` = `v` e1 v1 e2 v2 ... en `u`
                
                ------(grafo fortemente conexo)------
                
            - Grafo fracamente conexo:
                É um grafo que, se forem trocadas as suas arestas direcionadas por arestas não direcionadas o resultado é um grafo não direcionado conexo.
                
                ------(gif exemplo grafo fracamente conexo [transformação das arestas])------
    
    ### 2.8. Coloração de grafos:
    Pode-se também colorir as partes de um grafo. Quando um grafo é colorido, ele é dito **propriamente colorido** se e somente se todos os vértices adjacentes não tiverem a mesma cor.
    Sendo assim, mais uma terminologia interessante é o **número cromático**, que é o número mínimo de cores necessárias para colorir os vértices de um grafo propriamente.
    - Uma propriedade interessante que surge do conceito de **coloração própria** é que qualquer grafo completo `Kn` terá número cromático `n`.
        
    ------(exemplo de grafo propriamente colorido vs. um obviamente não propriamente colorido)------
    
    ### 2.9. Classificação dos grafos:
    * Grafos específicos com determinadas características recebem nomes especiais, devido justamente às suas propriedades, muitas vezes relacionadas a outros grafos. Sendo assim, é importante ter forte compreensão dos conceitos abaixo, principalmente pela utilidade deles quando relacionados aos conteúdos de Ciência da Computação.
        
        #### 2.9.1. Subgrafos:
        Um **subgrafo H** é um grafo em que os conjuntos de vértices e arestas são obrigatoriamente subconjuntos desses mesmos conjuntos de um **grafo G**.
        De maneira simplificada, um grafo só será subgrafo de outro se ele tiver apenas vértices e arestas que também estão no grafo maior, sem criar nenhum dado novo.
        ------(exemplos de grafo, subgrafos e grafos que não são subgrafos)------
        > Perceba que **não há apenas uma maneira de desenhar um mesmo grafo**, então visualizar se algum específico é subgrafo de outro pode se mostrar desafiador nessa representação. Entretanto, para encontrar maneiras formalizadas de representação de grafos, que resolve este problema, referencie [2.8. Representações diferentes de grafos].
        
        #### 2.9.2. Grafos Simples:
        Um grafo simples é um grafo que não possui laços nem múltiplas arestas conectadas a um mesmo vértice.
        
        ------(grafo simples vs grafo... não-simples)------
        
        #### 2.9.3. Grafos Completos:
        Um grafo é chamado de completo quando todos os vértices possuem uma aresta ligando todos os vértices. Em suma, é um grafo em que todas as relações possíveis estão presentes.
        * Esse tipo de grafo recebe geralmente o nome `K` seguido do número `n` de vértices.
        
        ------(Exemplos: K3, K5, K6 e K13)------
        
        * Uma propriedade interessante desse tipo de grafo é que o **número de arestas** equivale à `combinação entre as n arestas e 2`, como demonstrado abaixo.
        
        ------(Exemplo combinação)------
        
        #### 2.9.4. Grafos Complementares:
        Um grafo é um grafo complementar de outro quando a união de ambos resultar em um grafo completo. É a mesma maneira de pensar para qualquer tipo de elemento complementar, ou seja, é a parte que falta para esse elemento se tornar o todo.
        
        ------(grafo G, grafo complementar de G, K5 colorido com as cores de G e G')------
        
        Uma dúvida comum que pode surgir é como se representa o complementar de um grafo completo, mas na verdade é algo trivial, é apenas um grafo vazio.
        
        Afinal, seguindo a lógica anterior, um grafo `G` tem seu complementar como sendo `G' = Kv - G`, com `v` sendo o número de vértices de `G`. Logo, se `Kv` e `G` são idênticos, o resultado será 0, isso é, um grafo vazio.
        
        #### 2.9.5. Isomorfismos: 
        Dois grafos,`G1` e `G2` serão isomórficos se, e apenas se, existir uma **função bijetiva** que possa relacionar cada vértice de `V1` a `V2`, de maneira que todo par de vértices `{a,b}` pertencentes a `E1` tenham correspondentes em `E2`.
        * Formalmente, isso é descrito como:
        > G1 é isomorfo de G2 ⟺ ∃ f: V1 → V2 em que:
        1. f é bijetiva, e
        2. ∀a,b ∈ V1, {a,b} ∈ E1 ⟺ { f(a) , f(b) } ∈ E2.
        
        Isso se traduz visualmente como:
        
        ------(isomorfismo)------
        
        #### 2.9.6. Grafos bipartidos:
        Um grafo `G` será um chamado de bipartido se for possível dividir `V` em dois componentes menores: `Va` e `Vb`, fazendo com que cada `e ∈ E` tenha forma **{v1 ∈ Va , v2 ∈ Vb}** .
        Visualmente, o resultado dessa operação se mostra bem interessante, possibilitando inclusive compreender melhor o motivo desse tipo de grafo receber o nome "bipartido".
        
        ------(grafo em forma normal e bipartido)------
        
        - Não é todo grafo que possui essa propriedade. Em geral, sabe-se que um grafo só poderá ser **bipartido** se atender 2 requisitos:
            - Ele não pode conter ciclos ímpares.
            - Seu número cromático é menor ou igual a 2.
            
            ##### 2.9.6.1. Grafos completos bipartidos:
            Um grafo completo bipartido possui forma **`K`**`m,n`, em que `m` e `n` representam o número de vértices à esquerda e à direita do grafo bipartido. Esses vértices conectam-se com todos os outros do lado oposto, por isso recebe o nome de "grafo **completo** bipartido".
            
            ------(exemplo K2,3 e Km,n)------
            
            * Uma propriedade interessante desse tipo de grafo é que a **quantidade de arestas** será igual ao produto de `m` e `n` (é um teorema útil para o estudo de planaridade).
            
        #### 2.9.7. Grafos planares:
        Um grafo é **planar** se e somente se ele puder ser **desenhado sem arestas sobrepostas**.
        
        ------(exemplo com K4 vs. ele redesenhado)------
        
        Entretanto, esse método de detecção de planaridade não é exatamente conveniente, pois redesenhar um grafo diversas vezes, especialmente grafos grandes, é uma tarefa árdua. Por tanto, para provar que um grafo **não é planar**, este grafo precisa obrigatoriamente obedecer ao **Teorema de Kuratowski**:
        > Um  grafo será obrigatoriamente **não-planar** se ele tiver um subgrafo `homomórfico para K5 ou K3,3`.
        
        - **Nota**: Um grafo "homomórfico" é um grafo que pode ser **subdividido**, isso é, transformar uma aresta e = {v1,v2} qualquer em  duas arestas adicionando um terceiro vértice, e' = { v1, w } e e'' = { w, v2 };
            
            ------(Subdivisão de um grafo)------
            
        - Isso basicamente significa que, se for possível subdividir `K5` ou `K3,3` e chegar no grafo em questão, ou em algum subgrafo seu, esse grafo será considerado **não-planar**.
        
        > **Exemplo**: O grafo abaixo é planar?
        
        ------(grafo pra confundir)------
        
        Na verdade ele é sim. Uma maneira fácil de visualizar é rearranjar os vértices desse grafo para se parecer com o grafo K2,4
            
        Um teorema que deriva disso é que qualquer grafo `K2,m` será obrigatoriamente planar, e a intuição visual para isso é extremamente simples.
        
        ------(K2,m planaridade)------
        
        Entretanto, para testar o **Teorema de Kuratowski**, um melhor exemplo de grafo seria esse:
        
        ------(grafo full bagunçado)------
        
        Um bom método para resolver este tipo de problema é **escrever os graus dos vértices acima deles**, dessa maneira é possível selecionar se será possível buscar um grafo *homomórfico* a `K5` ou `K3,3`.
        
        ------(grafo full bagunçado com graus de vértices)------
            
        Nesse caso, não há vértices de grau 3, mas há dois vértices de grau 5 e um de grau 6, portanto o próximo passo é selecionar 5 vértices que estão mais próximos do grau do grafo escolhido, nesse caso, o grafo `K5`. Assim, basta tentar redesenhar esse conjunto de vértices na forma de `K5`, então conectá-los pelas arestas já definidas.
        Note que se não houver uma aresta conectando diretamente um vértice ao outro, pode-se utilizar o conceito de homomorfia e desenhar os vértices que estão no meio do caminho, portanto, basta os vértices serem conectados para desenhar uma  aresta, contanto que os vértices do "meio do caminho" não sejam os que já foram selecionados anteriormente.
        
        ------(procura por K5)------
        
        - Este é um processo não tão intuitivo, mas a prática contínua levará a um entendimento completo do algoritmo visual.
    
        > Foi possível visualizar que o grafo era sim uma subdivisão de `K5`, e portanto não era um grafo planar.
            
        * Por fim, a última dica sobre descobrir a planaridade de um grafo é procurar pelos graus dos vértices
            
        #### 2.9.8. Árvores:
        Um exemplo de tipo de grafo muito importante para a Ciência da Computação são árvores. Basicamente, **árvores são grafos conexos sem ciclos**. 
        - **Nota**: se um grafo for acíclico mas desconexo ele se chama **floresta**, afinal, serão basicamente múltiplas árvores.
        
        ------(árvore vs. floresta)------
        
        Uma propriedade interessante das árvores é que, para cada par de vértices existe **um único caminho** que os liga.
        
        Outra propriedade é o teorema de que o número de arestas é igual ao número de vértices menos 1. Esse teorema pode ser provado por **indução matemática**, mas provar os teoremas apresentados está fora do escopo desta wiki.
        
        > |E| = |V| - 1
        
        ------(exemplo com uma árvore binária)------
        
    ### 2.10. Representações diferentes de grafos
    
    ------(grafo para referenciar abaixo)------
    
    * Computadores podem auxiliar os seres humanos a estudar as propriedades dos grafos e até mesmo possibilitar uma melhor visualização com a utilização de ferramentas gráficas. Entretanto, computadores não pensam como humanos, portanto é necessário representar grafos de uma maneira que ele possa processar os dados relacionados, e para isso serão citadas 3 alternativas interessantes:
    
        #### 2.10.1. Lista de Adjacências:
        Provavelmente a maneira mais comum de representar grafos em computadores, justamente pelo modo como é fácil obter informações sobre os vértices adjacentes de algum vértice do grafo, que é um tipo de informação imensamente útil para utilizar nos algoritmos de grafos.
        Além disso, esse tipo de representação se mostra mais versátil quando, ao se trabalhar com grafos de larga escala e que sabe-se que um único vértice não possui um enorme número de arestas ligadas a ele, pois dessa maneira esse método irá **consumir menos memória** do que uma matriz de adjacências, por exemplo.
        A ideia principal é mapear cada vértice a uma lista de seus vizinhos, ou, utilizando a terminologia definida acima, os vértices são mapeados a uma lista contendo todos os outros vértices adjacentes a esse primeiro.
        
        ------(adjacency list of the graph above)------
        
        > Lista relacionada ao grafo acima.
        
        #### 2.10.2. Matriz de Adjacências:
        Outra maneira de representar um grafo é utilizando uma matriz de adjacências. Essa matriz mapeia os vértices entre si, de modo que o número que liga a coluna à linha em questão é o número de arestas ligando esses dois vértices.
        * No exemplo abaixo, a matriz é de um grafo não direcionado, então entradas acabam sendo repetidas. de maneira espelhada na diagonal, mas para um grafo direcionado, já se sabe que a aresta `ab` é diferente da aresta `ba`.
        
        ------(adjacency matrix of the graph above)------
        
        > Matriz relacionada ao grafo acima.
        
        * Exemplo com grafo direcionado:
        
        ------(self explanatory isnt it)------
        
        > Note que os "*vértices de partida*" estão à esquerda da matriz, e os "*vértices de chegada*" estão acima dela.
        
        #### 2.10.3. Conjunto das Arestas:
        O conjunto de arestas de um grafo pode ser utilizado por si só para definir o grafo como um todo. Entretanto, esse método de representação é mais incomum, pois apresenta maiores dificuldades na extração de informações, mas ainda é possível.
        
        ------(edge set of the graph above)------
        
        > Conjunto de arestas relacionado ao grafo acima.
    
## 3. Busca em grafos:
* Um tipo de algoritmo essencial é o algoritmo que tem por objetivo encontrar determinado valor em um grafo. Em especial citam-se dois tipos de busca:
    
    #### 2.9.1.1. Busca em Largura
    Uma maneira de realizar uma busca em um grafo é com a **busca em largura**.
    Ese método consiste em encontrar todos os vértices adjacentes de um vértice de partida, então selecionar um desses novos vértices e repetir o processo. Caso todos os vizinhos já tenham sido descobertos e esses vizinhos não tenham nenhum outro adjacente, o vértice de partida será o último vértice que ainda tinha vizinhos inexplorados.
    
    ------(gif largura)------
    
    Essa solução para a busca em grafos é especialmente útil para casos como **busca por possíveis infectados por covid**, utilizando de um exemplo atual, pois, caso a rede social de uma pessoa infectada possa ser representada por um grafo é possível realizar esse método de busca para encontrar as pessoas próximas que podem ter sido infectadas também.

    #### 2.9.1.2. Busca em Profundidade:
    Suponha que o objetivo de um algoritmo é visitar **todos** os vértices de um grafo, portanto é possível utilizar, entre outros, o algoritmo de **busca em profundidade**. Esse tipo de busca se diferencia da busca em largura porque no processo, após se descobrir um novo vértice, esse mesmo novo vértice será o ponto de partida para o próximo passo. E caso chegue-se a um ponto final em que o único caminho é retornar, então se retorna até o último vértice com um vizinho não explorado ainda.
    
    ------(esse gif vai ser complexinho)------
    
    > Note que, quando todos os vizinhos de um vértice forem descobertos, os vértices da imagem acima ficam vermelhos, para facilitar a visualização.
    
    * Perceba que a ordem específica dos vértices escolhidos não importa, basta que o método de busca utilize o último vértice descoberto como ponto de partida, que é o que define uma busca em profundidade.
    
    Esse tipo de algoritmo pode ser utilizado na detecção de ciclos em um grafo. Basta que, ao aplicar *DFS* (*Depth First Search* do inglês) faça-se uma checagem para confirmar que um vértice já explorado não faz parte das opções de exploração de um novo vértice.
    
    ------(gif detecção de ciclo)------

## 4. Principais Marcos
* Teoria dos Grafos envolve conceitos mais antigos do que sua extensa aplicação em programação e computadores digitais. Especificamente, essa área da Matemática surge ainda no século XVIII, portanto é interessante compreender como esse campo de estudo de mais de dois séculos de idade evoluiu até chegar no estado atual.
    ### 4.1. Surgimento (~1735):
    A ideia de grafos foi introduzida originalmente pelo matemático suíço **Leonhard Euler**, ainda no século XVIII, na tentativa de resolver o famoso problema das pontes de Königsberg. Esse problema surgiu quando Euler se questionou se seria possível fazer uma caminhada que passasse por todas as 7 pontes da cidade apenas uma vez, e com essa ideia ele desenhou pela primeira vez a representação de um grafo moderno, isto é, um conjunto de vértices ligados por arestas. (Imagem abaixo)
    
    ------(Ilustração das pontes e o grafo)------
    ### 4.2. Desenvolvimento dos primeiros conceitos (século XIX):
    No século XIX diversos estudiosos progrediram com novos conceitos para a Teoria dos Grafos, conceitos como grafos hamiltonianos, árvores e grafos planares. De certa maneira, a maioria da terminologia que existe hoje foi formulada pelos grupos de estudiosos do século XIX. Problemas interessantes também foram propostos neste século, como a decisão se o grafo K3,3 é planar e também o problema das quatro cores, proposto por **Francis Guthrie**, que não possui até hoje uma resolução realmente eficiente.
    
    ### 4.3. Resolução do problema das quatro cores (1879, 1976 e 1994):
    O **problema das quatro cores** foi "resolvido" inicialmente em 1879 por Alfred B. Kempe, mas essa solução foi provada incorreta, e quase um século depois quando Kenneth Appel e Wolfgang Haken revisaram o problema e providenciaram uma solução complexa. Entretanto, uma nova solução, correta, e muito mais simples foi produzida por Neil Roberton, Daniel Sanders, Paul Seymour, e Robin Thomas no ano de 1994.

    ### 4.4. Cenário atual:
    Na atualidade, a Teoria de Grafos está muito presente na área da computação, por facilitar a resolução de mais novos e mais complexos problemas, muitas vezes só possíveis de resolver devido ao próprio avanço da tecnologia de computadores. Esse é um campo muito estudado na Matemática Discreta, mas faz aparições frequentes na Ciência da Computação também, justamente pela sua utilidade quanto aos problemas modernos de computação.

    Evolução do tópico: desde a sua criação até o cenário atual
    consulte a Linha de Tempo disponibilizada no Bloco 1 e veja os materiais sobre História da Computação para começar a construir a linha de tempo específica do seu tema. Criar um infográfico é uma boa forma de apresentar os principais marcos.

## 5. Grandes Nomes

* No desenvolvimento da Teoria dos Grafos, certos estudiosos se destacaram entre os demais devido à importância de suas contribuições à área. Dentre eles, pode-se destacar:

    ### 5.1. Leonhard Euler (1707 ~ 1783):
    
    ------(imagem de Euler)------
    
    * Leonhard Euler foi um matemático (assim como físico, astrônomo, geógrafo, um lógico e engenheiro) suíço que nasceu em 15 de Abril de 1707, em Basel na Suíça.
    * Ele era filho de Paul III Euler, um pastor da Igreja Reformista, e de Marguerite Brucker, filha de outro pastor, e ele era o mais velho de quatro filhos. Entretanto, mesmo que tenha nascido em Basel, a maior parte de sua infância se passa em Riehen, cidade para onde seus pais se mudaram.
    * Paul era amigo de Jacob Bernoulli, matemático importante da época que o ensinou muito sobre Matemática, e teve grande influência no Euler enquanto ele ainda era criança.
    * A educação formalizada de Euler se inicia quando ele se muda para Basel, morando com a avó materna. Aos 13 anos de idade, em 1720, Euler entra para a Universidade de Basel, e em 1723 ele se torna um Mestre em Filosofia quando submete uma redação comparando as filosofias de René Descartes e Isaac Newton.
    * Como era plano de seu pai que Leonhard virasse pastor, ele entra para a faculdade de teologia, na Universidade de Basel. Entretanto, nos finais de semana ele tinha aulas de Matemática com Bernoulli, que rapidamente percebeu seu talento para a Matemática. Então, Euler consegue permissão de seu pai para se tornar matemático.
    * Euler é conhecido como um dos matemáticos mais importantes da história, e provavelmente o mais importante de todo o século XVIII.
    * Quanto à **Teoria de Grafos**, Leonhard Euler foi o fundador dessa área de estudos, quando, para ter certeza se era possível passar por todas as pontes de Königsberg em uma caminhada, sem passar duas vezes pela mesma ponte. E assim, Euler faz a primeira ilustração do que conhece-se hoje como grafo, assim como define diversos conceitos base essenciais para o desenvolvimento da área por outros matemáticos mais à frente na linha do tempo.
    * Euler falece inesperadamente em São Petersburgo, em 18 de setembro de 1783, enquanto discutia sobre a descoberta de Urano ele colapsou por causa de uma hemorragia cerebral e morreu.
    
    ### 5.2. Francis Guthrie (1831 ~ 1899):
    
    ------(imagem de Guthrie)------
    
    * Francis Guthrie foi um matemático e botânico que nasceu em Londres, em 22 de Janeiro de 1831, filho de Alexander David Guthrie e Kitty Thompson.
    * Guthrie estudou Matemática sob a tutela de **Augustus De Morgan**, outro matemático de grande renome. 
    * Enquanto coloria um mapa dos condados da Inglaterra, ele acabou se perguntando sobre o número mínimo de cores para que nenhum condado adjacente tenha a mesma cor. Neste caso, a resposta foi quatro, e ele então propõe a questão se quatro cores serviriam de solução para colorir qualquer outro mapa sob as mesmas condições restritivas. Daí surge o **problema das quatro cores**, que é a sua grande contribuição na área da Teoria dos Grafos.
    * Ademais, Guthrie se muda para a África do Sul em 1861, e toma o posto de mestre de matemática no Colégio Graaff-Reinet, onde ministrou aulas públicas sobre botânica. Até que em 1875, ele se muda para *Cape Town*, onde ele eventualmente se torna professor de Matemática no Colégio Sul Africano, que depois mudaria o nome para **Universidade de Cape Town**. Finalmente, em 1898, Francis Guthrie se aposenta e passa o resto de seus dias em sua fazenda na cidade de Raapenberg.
        
    ### 5.3. Docentes DInf
    
        Adicionalmente aos dois nomes, apresentar também nome de pelo menos um docente do DInf que atue no tópico ou em temas relacionados, e indicar as disciplinas da graduação que abordam o tema.
    
        Veja os materiais do Bloco I, consulte a página do DInf e do PPGInf, pesquisa os demais materiais disponibilizados e links externos. Se ainda assim tiver dificuldades, peça dicas no Fórum de Dúvidas.
        O Google Scholar é uma bom local para procurar por nomes em diversas áreas também. Exemplo de pesquisa por nomes que trabalham com "Artificial Intelligence"

## 6. Aplicações
- A **Teoria dos Grafos** possui inúmeras aplicações por todos os campos de estudo voltados à uma área mais técnica. Dentre algumas dessas aplicações práticas, pode-se citar dois exemplos.

    ### 6.1. Redes de computadores:
    - Redes de computadores são representadas facilmente por grafos, tanto é que, seguindo essa lógica, toda a internet pode ser representada por um grafo, já que ela consiste em um enorme conjunto de máquinas operando simultaneamente e se comunicando entre si.
   - Tudo o que é necessário é representar cada máquina como um vértice, e a conexão entre elas como uma aresta. Por fim, está desenvolvido um dos maiores grafos do mundo.
    
    ------(imagem rede de computadores)------

    - Na verdade, na última década o ramo de **Machine Learning** e **Inteligência Artificial** cresceram imensamente, e com elas também fica mais comum ver termos como **deep learning** e **redes neurais**. Especifiacmente sobre rede neurais, sabe-se que a maneira mais comum de representá-las (e isso é válido para quase todo tipo de rede) é justamente com grafos.
    
    ------(exemplo de rede neural micro)------
    
    > Faça um experimento, depois de ler a visão geral dessa *wiki*, vá à sua ferramenta de busca e procure por imagens de redes neurais, você notará a familiaridade das imagens.

    ### 6.2. Sudoku?
    Um problema que não é tão intuitivamente relacionado à Teoria dos Grafos é a resolução de uma tabela de *sudoku*;
    
    > **Conhecimento da terminologia apresentada é requerido para entendimento dessa explicação.**
    
    - Inicialmente, sabe-se que o objetivo de sudoku é preencher uma tabela 9x9 com números de 1 a 9, de maneira que qualquer subtabela não pode ter números repetidos, e o mesmo vale para todas as linhas e colunas.
    - Na verdade, computadores podem resolver esse problema eficientemente utilizando teoria dos grafos. A única condição é que sudoku precisa ser representado ligeiramente diferente, ou seja, deve ser traduzido para o jargão da teoria dos grafos.
    - O que se pode fazer é dizer que cada número é representado por uma cor, assim o grafo será preenchido com 9 cores distintas.
    
    ------(9 números, cada um de cada cor)------
    
    - Entretanto, ainda é preciso saber **como representar isso em um grafo**. Para isso, é possível representar cada um daquelas subtabelas como grafos K9.
    
    ---------(1 K9 e depois todos os 9 K9)---------
    
    - Porém há um problema com essa representação, e é o fato de que as subtabelas, como o nome implica, não são coisas separadas, mas partes de um todo. Por sorte, essa parte é relativamente intuitiva, basta que todas as linhas e colunas sejam representadas pelas arestas verticais e horizontais, assim será possível representar a dependência desses valores.
    
    ------(gif conectando os treco)------
    
    - Acontece que acabamos de transformar o problema de sudoku em um problema muito conhecido da teoria dos grafos, e ao aplicar um algoritmo extremamente elegante é possível resolver a tabela.
    
    ------(gif resolução da tabela)------
    
    > **Nota**: esse exemplo foi retirado de um vídeo do *YouTube*: https://www.youtube.com/watch?v=LFKZLXVO-Dg.
    (eu simplesmente não consegui elaborar um exemplo melhor e mais interessante sem ficar exageradamente complexo)
    
    - O programador que não estudou estruturas de dados mais complexas e nem teoria dos grafos terá a ideia brilhante de representar a tabela com uma matriz, consequentemente, a solução proposta por esse programador seria, na maioria dos casos, extremamente ineficiente, o que ressalta a importância da teoria dos grafos, pois eficiência é extremamente essencial quando se elabora um algoritmo.
    
## 7. Impacto Social e Questões Éticas
- Durante a tentativa de provar o **teorema das quatro cores**, Kenneth Appel e Wolfgang Haken foram muito criticados pelo uso de computadores para encontrar a solução para o problema. Anos depois, uma solução muito mais elegante foi proposta justamente fazendo o uso de computadores.
- A questão é que, se essas críticas (vindas de preconceito) quanto ao estudo com o uso de computadores não tivessem sido feitas será que as soluções poderiam ter sido melhoradas ainda antes?
- Isso levante um questionamento interessante: será possível que mesmo hoje algum preconceito, não necessariamente relacionado ao uso de computadores, mas qualquer preconceito em qualquer meio de estudo? Será possível que muito conhecimento esteja sendo limitado, atrasado ou até mesmo perdido devido às críticas muitas vezes infundadas?
- No geral, esse ponto do desenvolvimento da teoria dos grafos levanta questionamentos que vão muito além da Ciência da Computação e da Matemática. São questionamentos que se estendem para qualquer área do conhecimento. Essa possível perda de conhecimento é algo preocupante para o desenvolvimento de estudos, e idealmente esse tipo de limitação jamais poderia existir.

## 8. Desafios
* Ainda atualmente, mesmo com máquinas com poder de processamento quase incompreensíveis para os estudiosos de teoria dos grafos do início do século passado, o fato é que existem desafios que foram propostos e estão sem uma solução ideal. Desses desafios cita-se um proposto ainda no século XIX:
    ### 8.1. Problemas de coloração (problemas... "NP-Completos"?):
    O problema das quatro cores se originou no ano de 1852, quando **Francis Guthrie** estava colorindo o mapa dos condados de modo que **cada condado não pode ter outro condado com a mesma cor adjacente a ele**, e Guthrie se perguntou qual seria o número mínimo de cores que ele poderia utilizar para colorir o mapa com as restrições acima. Pelas suas tentativas, ele concluiu que **3 cores não eram suficientes, mas 4 eram**; e então ele se pergunta se quatro cores seriam o suficiente para qualquer mapa, daí surge o famoso **problema das quatro cores**.
    A simplicidade de uma pergunta como essa levou incontáveis matemáticos a tentar achar uma solução para o problema, até mesmo matemáticos de renome mundial, e geralmente não ocorria sucesso. 
    Em 1879, Alfred Bray Kempe propôs uma solução que foi aclamada como correta, mas essa solução foi desprovada 11 anos depois por Percy John Heawood. É raro um resultado incorreto ser publicado e adotado como correto, sem ser desafiado por tanto tempo, fato que auxilia o entendimento das reais dificuldades de trabalhar nesse problema.
    Apenas em 1976, com Kenneth Appel e Wolfgang Haken que uma prova do problema das quatro cores finalmente foi proposta, mas não foi muito bem aceita, pois foram necessárias 1200 horas de computação para completá-la. Em 1976, utilizar um computador para provar algo era considerado ultrajante, e muitos debates eram feitos sobre isso.
    Em 1996, quando os matemáticos já estavam mais confortáveis com o uso de computadores para provar algo, um grupo de pesquisadores elaborou uma nova prova, muito mais simplificada que a original. Entretanto, a teoria é que certamente deve ser possível provar, de maneira ainda mais simplificada, o problema das quatro cores, que foi elevado para o **teorema das quatro cores**.
    O desafio, atualmente, é que o problema das quatro cores se mostra "**NP-Completo**", isso é, não existe um algoritmo eficiente para resoolver o problema. Os problemas NP-Completos são os ditos mais difíceis de resolver, e muito provavelmente não podem ser resolvidos em tempo polinomial. 
    A real razão da busca de um algoritmo que possa ser executado em tempo polinomial é que, se **um, e apenas um** problema NP-Completo puder ser resolvido rapidamente/eficientemente, então poderiam ser usados algoritmos para resolver qualquer problema NP rapidamente.
    Atualmente, no Brasil, uma dissertação de mestrado de Ciência da Computação da UTFPR teve avanço depois de mais de uma década desde as últimas evoluções. Nessa dissertação, Luis Gustavo Gonzaga descreve que existirá sim uma solução eficiente caso o grafo em questão for pertencente à interseção das classes intervalos e *split*.
    Para compreensão melhor do que foi descrito recomenda-se a leitura de:
    - http://www.utfpr.edu.br/noticias/geral/pesquisador-avanca-em-um-problema-computacional-apos-35-anos-sem-solucao
    - https://pt.wikipedia.org/wiki/Complexidade_computacional
    - https://pt.wikipedia.org/wiki/NP-completo

## 9. Referências
* TODO: **FAZER** as imagens ;-; (e apresentar link do repositório depois)
    - Todas as imagens ~~foram~~ serão produção própria (menos as das pessoas, claro)
    - Tenho quase certeza que a formatação correta para referências não é bem assim, !"verificar depois".
     
    [1] STANFORD. *Basic Graph Algorithms*. [2015]. Disponível em: https://web.stanford.edu/class/cs97si/06-basic-graph-algorithms.pdf. Acesso em: 06 de Outubro de 2021.
    
    [2] WIKIPÉDIA. Francis Guthrie. [2021]. Disponível em: https://en.wikipedia.org/wiki/Francis_Guthrie. Acesso em: 05 de Outubro de 2021.
    
    [3] WIKIPÉDIA. Leonhard Euler. [2021]. Disponível em: https://en.wikipedia.org/wiki/Leonhard_Euler. Acesso em: 05 de Outubro de 2021.
    
    [4] TOWARDS DATA SCIENCE. *Graph Theory - History and Overview*, Jesus Najera. Disponível em: https://towardsdatascience.com/graph-theory-history-overview-f89a3efc0478. Acesso em: 04 de Outubro de 2021.
    
    [5] PROBLEMAS DE COLORAÇÃO EM GRAFOS. Sheila Morais de ALmeida. [2020]. Disponível em: https://sheilaalmeida.pro.br/coloracao.html. Acesso em: 03 de Outubro de 2021.
    
    [6] DISCRETE MATHEMATHICS FOR COMPUTER SCIENCE. Gary Haggard, John Schlipf, Sue Whitesides. [2005]. Disponível em: http://www2.cs.uh.edu/~arjun/courses/ds/DiscMaths4CompSc.pdf. Acesso em: 01 de Outubro de 2021.
    
    [7] WOLFRAM, MATHWORLD. Eric W. Weisstein. [2021]. Disponível em: https://mathworld.wolfram.com/CompleteGraph.html. Acesso em: 02 de Outubro de 2021.
    
    [8] GRAPH THEORY WITH APPLICATIONS. J. A. Bondy, U. S. R. Murty. [1976]. Disponível em: http://www.maths.lse.ac.uk/Personal/jozef/LTCC/Graph_Theory_Bondy_Murty.pdf. Acesso em: 30 de Setembro de 2021.
    
    [9] THE EASIEST UNSOLVED PROBLEM IN GRAPH THEORY. Sergei Ivanov. [2021]. Disponível em: https://www.cantorsparadise.com/the-easiest-unsolved-problem-in-graph-theory-fa3a7f26181b. Acesso em: 30 de Setembro de 2021.

## 10. Extenda sua leitura

- Teoria dos Grafos, Bondy Murty: 
    http://www.maths.lse.ac.uk/Personal/jozef/LTCC/Graph_Theory_Bondy_Murty.pdf
- Matemática para Ciência da Computação:
    http://www2.cs.uh.edu/~arjun/courses/ds/DiscMaths4CompSc.pdf
- O Livro da Prova (n sei exatamente se a tradução teria como não ser ambígua):
    http://www.people.vcu.edu/~rhammack/BookOfProof/Main.pdf
- Estruturas de Dados:
    https://www.geeksforgeeks.org/data-structures/
- Algoritmos Básicos de Grafos:
    https://web.stanford.edu/class/cs97si/06-basic-graph-algorithms.pdf
- Vídeos sobre Matemática Discreta, TrevTutor:
    - https://www.youtube.com/watch?v=tyDKR4FG3Yw&list=PLDDGPdw7e6Ag1EIznZ-m-qXu4XX3A0cIz
    - https://www.youtube.com/watch?v=HkNdNpKUByM&list=PLDDGPdw7e6Aj0amDsYInT_8p6xTSTGEi2

## 11. Autoria

Vou deixar isso por último
