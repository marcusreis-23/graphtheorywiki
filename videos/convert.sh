# if you're wondering, I had to use ffmpeg because, for some reason, the gif rendering from
# manim seemed to be broken, and I had no clue on how to fix it, but the videos worked fine
# so I just converted all the mp4 diles to gif files (which is pronounced "gif" and not "gif")
for FILE in ./*.mp4 
do    
    # the downscale is becaude I didn't need it in high quality, 720 with lanczos did the trick
    ffmpeg -i $FILE -vf "fps=30,scale=720:-1:flags=lanczos" ${FILE/.mp4/.gif}
done
