from manimlib import *
from math import sin, cos
COLORS_ARR = [BLUE_D,TEAL_E,GREEN_D,YELLOW_D,GOLD_C,RED_E,MAROON_E, PURPLE_C, GREY_C, GREY_BROWN,LIGHT_BROWN,PINK,ORANGE]
class GraphVertex:
	def __init__(self, name='', pos=ORIGIN, radius=0.5):
		self.name      = TexText(str(name))
		self.position  = pos
		self.circle    = Circle(radius=radius)
		self.size      = radius
		self.name.move_to(pos)
		self.circle.move_to(pos)
		self.adjacents = []
		self.edges     = []
		self.visited   = False

	def connect_node(self, next_v, color, directed=False, curved=False, angle=-PI/2, loop=False):
		line = Line(self.position, next_v.position)
		unit_vector = line.get_unit_vector()
		start, end = line.get_start_and_end()
		if directed:
			new_start = start + unit_vector * self.size*0.6
			new_end = end - unit_vector * next_v.size*0.6
			if curved:
				new_start = new_start*0.9
				new_end = new_end*0.9
				line = CurvedArrow(new_start, new_end, width=8, angle=angle, arc_center=ORIGIN)
			else:
				line = Arrow(new_start, new_end)
		else:
			new_start = start + unit_vector * self.size
			new_end = end - unit_vector * next_v.size
			if curved:
				line = ArcBetweenPoints(new_start, new_end, angle=angle)
			else:
				line = Line(new_start, new_end)

		if next_v not in self.adjacents:
			self.adjacents.append(next_v)
			next_v.adjacents.append(self)
		if line not in self.edges:
			self.edges.append(line)
			next_v.edges.append(line)
		line.set_color(color)
		return line

	def highlight_vertex(self,color):
		highlight = Circle(radius=self.size).move_to(self.position)
		highlight.set_stroke(color=color, width=self.size*28)
		return highlight
	def highlight_edge(self,other,color):
		if other in self.adjacents:
			line = Line(self.position, other.position)
			unit_vector = line.get_unit_vector()
			start, end = line.get_start_and_end()
			new_start = start + unit_vector * self.size*1.1
			new_end = end - unit_vector * other.size*1.1
			line = Line(new_start, new_end)
			line.set_stroke(color=color, width=35*self.size)
			return line
		return Line(ORIGIN,ORIGIN)
	def dfs(self,target=None,found_color='#0000FF',ecolors=GREEN_SCREEN,vcolors='#FF0000',first_color=None):
		found = False
		marked_stuff = VGroup(self.highlight_vertex(vcolors if not first_color else first_color))
		self.visited = True
		if target == None:
			objective = 'X'
		else:
			objective = target.name.get_tex()
		if self.name.get_tex() != objective:
			for i,adj in enumerate(self.adjacents):
				if adj.visited:
					continue
				marked_stuff.add(self.highlight_edge(adj,ecolors))
				if adj.name.get_tex() != objective:
					marked_stuff.add(adj.highlight_vertex(vcolors))
				else:
					adj.visited = True
					found = True
					marked_stuff.add(adj.highlight_vertex(found_color))
					marked_stuff.add(self.highlight_edge(adj,ecolors))
					break

				if not adj.visited:
					if target == None:
						m=adj.dfs(target,found_color=found_color,vcolors=vcolors,ecolors=ecolors)
					else:
						m,found=adj.dfs(target,found_color=found_color,vcolors=vcolors,ecolors=ecolors)
					marked_stuff.add(m)
				if found:
					break
		if target != None:
			return marked_stuff, found
		else:
			return marked_stuff

	def dfs_cicle(self,previous,found_color='#0000FF',vcolors='#FF0000'):
		found = False
		marked_stuff = VGroup(self.highlight_vertex(vcolors))
		self.visited = True
		for i,adj in enumerate(self.adjacents):
			if adj.name.get_tex() == previous.name.get_tex():
				continue
			marked_stuff.add(self.highlight_edge(adj,GREEN_SCREEN))
			marked_stuff.add(adj.highlight_vertex(vcolors))
			if adj.visited:
				marked_stuff.add(adj.highlight_vertex(found_color))
				marked_stuff.add(self.highlight_edge(adj,GREEN_SCREEN))
				return marked_stuff, True
			else:
				m,found=adj.dfs_cicle(self,found_color=found_color,vcolors=vcolors)
				marked_stuff.add(m)
			if found:
				break
		return marked_stuff, found
	def bfs(self,target=None,found_color='#0000FF',ecolors=GREEN_SCREEN,vcolors='#FF0000',first_color=None, count_targets=False):
		marked_stuff = VGroup(self.highlight_vertex(vcolors if not first_color else first_color))
		self.visited = True
		found = False
		queue = self.adjacents.copy()
		previous = self
		next_change = len(self.adjacents)
		count = 0
		prox = self.adjacents[0]
		if target == None:
			objective = 'X'
		else:
			objective = target.name.get_tex()
		done = []
		while len(queue) > 0:
			next_v = queue.pop(0)
			found = next_v.name.get_tex() == objective
			if not next_v.visited:
				next_v.visited=True
				if found:
					count += 1
				done.append(next_v)
				marked_stuff.add(previous.highlight_edge(next_v,ecolors))
				marked_stuff.add(next_v.highlight_vertex(found_color if found else vcolors))
				for adj in next_v.adjacents:
					queue.append(adj)
				next_change -= 1
			if next_change <= 0:
				previous = prox
				if len(done)!=0:
					prox = done.pop(0)
				for a in previous.adjacents:
					if not a.visited:
						next_change += 1
			if found and not count_targets:
				break
		if target == None:
			return marked_stuff
		elif count_targets:
			return marked_stuff, count
		else:
			return marked_stuff, found
def generate_graph_walk(walk, vertex_group, vcolors=RED_C,ecolors=GREEN_SCREEN,last_color=RED_C,first_color=RED_C):
	highlights = VGroup()
	for i in range(len(walk)-1):
		highlights.add(VGroup(
			vertex_group[walk[i]].highlight_vertex(vcolors if first_color!=vcolors else first_color), 
			vertex_group[walk[i]].highlight_edge(vertex_group[walk[i+1]],ecolors), 
		))
		if i == len(walk)-2:
			highlights.add(VGroup(vertex_group[walk[i+1]].highlight_vertex(vcolors if last_color==vcolors else last_color)))
	return highlights


def generate_mobjects(nodes, edges, node_color, stroke_color, name_color=BLACK, scale=1, vci={}, op=1, colored=False):
	node_mobjects = []
	edge_mobjects = []
	for i,node in enumerate(nodes):
		node.circle.set_fill(color=(vci[node.name.get_tex()] if node.name.get_tex() in vci else (COLORS_ARR[i] if colored else node_color)), opacity=op)
		node.circle.set_stroke(color=stroke_color)
		node.name.set_color(color=name_color)
		node_mobjects.append(VGroup(node.circle, node.name))

	for edge in edges.values():
		if (type(edge) == type(CurvedArrow(ORIGIN,ORIGIN))):
			edge.set_stroke(width=11*scale)
		else:	
			edge.set_stroke(width=7*scale)
		#edge.set_color(color=stroke_color)
		edge_mobjects.append(edge)
	return VGroup(*edge_mobjects), VGroup(*node_mobjects), nodes

def graph_gen(nodes_info, edges_info, color=PURPLE_C, direct=False, size=1, vertex_colors={}, edge_colors={}, border_color=GREY_E, data=False, curved_edges=[]):

	size = size / 2
	
	edges = {}
	nodes = []

	for node in nodes_info:
		nodes.append(GraphVertex(name=node['name'] if 'name' in node else '', pos=node['pos'], radius=size))
	for i in range(0, len(edges_info)):
		if edges_info[i] in edges.keys():
			continue
		edges[edges_info[i]] = nodes[edges_info[i][0]].connect_node(
			nodes[edges_info[i][1]],
			edge_colors[edges_info[i]] if edges_info[i] in edge_colors else GREY_E,
			directed=direct,
			curved=edges_info[i] in curved_edges or (edges_info[i][1],edges_info[i][0]) in edges,
			loop=nodes[edges_info[i][0]] == nodes[edges_info[i][1]]
		)

	edges, nodes, vertices_raw = generate_mobjects(nodes, edges, color, border_color, vci=vertex_colors)

	graph = VGroup(nodes, edges)
	if data:
		return graph, vertices_raw
	else:
		return graph

def k_graph_gen(n, color=PURPLE_C, direct=False, size=1, vertex_colors={}, edge_colors={}, border_color=GREY_E, colored=False):
	size = size / 2
	edges = {}
	nodes = []
	for i in range(0,n):
		r = 2 if n < 9 else n*0.3
		nodes.append(GraphVertex(name=str(i+1)))
		
		nodes[i].circle.move_to(RIGHT*r * cos(2 * PI * i / n) + UP*r * sin(2 * PI * i / n))
		nodes[i].name.move_to(RIGHT*r * cos(2 * PI * i / n) + UP*r * sin(2 * PI * i / n))
		nodes[i].position = (RIGHT*r * cos(2 * PI * i / n) + UP*r * sin(2 * PI * i / n))
	
	for i in range(0,n):
		for j in range(0,n):
			if i != j and not ((i,j) in edges) and not ((j,i) in edges):
				edges[(i, j)] = nodes[i].connect_node(
					nodes[j], edge_colors[(i,j)] if (i,j) in edge_colors else GREY_E,
			)

	nodes, edges, info = generate_mobjects(nodes, edges, color, border_color, vci=vertex_colors, colored=colored)

	kgraph = VGroup(edges, nodes)
	return kgraph
#############################################################################################
#																							#
#										IMAGES												#
#																							#
#############################################################################################

class ExampleGraph123(Scene):
	def construct(self):
		# node_data = [
		# 	{'pos': UP*2},
		# 	{'pos': LEFT*2.5},
		# 	{'pos': RIGHT*2.5},
		# 	{'pos': LEFT*1.3 + DOWN*2.5},
		# 	{'pos': RIGHT*1.3 + DOWN*2.5}
		# ]
		# edges_info = [
		# 	(0,1),(0,2),(0,3),(0,4),(1,2),(1,3),(1,4),(2,3),(2,4),(3,4)
		# ]
		node_data = [
			{'pos': LEFT*2+UP*2},
			{'pos': LEFT*2},
			{'pos': LEFT*2+DOWN*2},
			{'pos': RIGHT*2},
		]

		edges_info = [
			(0,1),(1,0),(1,2),(2,1),(0,3),(1,3),(2,3)
		]

		graph = graph_gen(node_data, edges_info, size=0.75)
		self.add(graph)

class GraphWithDefinitions(Scene):
	def construct(self):
		
		title = Text('Representação visual de G', color=GREY_E)
		title.move_to(TOP*0.8)
		self.add(title)

		nd = [
			{ 'name': 'v0', 'pos': LEFT*1.2 },
			{ 'name': 'v1', 'pos': UP*1.2 },
			{ 'name': 'v2', 'pos': DOWN*1.2 },
			{ 'name': 'v3', 'pos': RIGHT*1.2 },
		]

		ei = [ (0,1), (2,3), (0,2) ]

		graph = graph_gen(nd, ei)
		self.add(graph)

class BinaryTree(Scene):
	def construct(self):
		nd = [
			{ 'pos': UP*2 },
			{ 'pos': UP*1+LEFT },
			{ 'pos': UP*1+RIGHT },
			{ 'pos': DOWN*0.2+LEFT*2 },
			{ 'pos': DOWN*0.2+RIGHT*2 },
			{ 'pos': DOWN*0.2+LEFT*0.8 },
			{ 'pos': DOWN*0.2+RIGHT*0.8 },
			{ 'pos': DOWN*2+LEFT*2.8 },
			{ 'pos': DOWN*2+RIGHT*2.8 },
			{ 'pos': DOWN*2+LEFT },
			{ 'pos': DOWN*2+RIGHT },
		]

		ei = [ (0,1), (0,2), (1,3), (1,5),(3,7), (3,9), (2,6), (2,4), (4,8), (4,10) ]

		graph = graph_gen(nd, ei, color=GREEN_D)
		self.add(graph)

class NoEdges(Scene):
	def construct(self):
		nd = [
			{'pos': ORIGIN},
			{'pos': LEFT*2+UP*2},
			{'pos': RIGHT*2},
			{'pos': RIGHT*3+DOWN},
		]

		graph = graph_gen(nd, [])

		self.add(graph)

class SimpleGraph(Scene):
	def construct(self):
		nd = [
			{'pos': RIGHT/3+UP*2},
			{'pos': LEFT*3+UP},
			{'pos': RIGHT*3+DOWN/2},
			{'pos': LEFT/5+DOWN*3},
		]

		ei = [(0,1),(0,2),(0,3),(1,2)]

		g = graph_gen(nd, ei, color=GREEN_E)

		self.add(g)

class NotSimpleGraph(Scene):
	def construct(self):
		nd = [
			{'pos': RIGHT/3+UP*2},
			{'pos': LEFT*3+UP},
			{'pos': RIGHT*3+DOWN/2},
			{'pos': LEFT/5+DOWN*3},
		]

		ei = [(0,1),(0,2),(0,3)]

		g = graph_gen(nd, ei)

		#loop = Ellipse(color=RED_E, stroke_width=10)
		#loop.move_to(LEFT+DOWN*3)
		error = TexText('Grafos simples não têm aresta dupla!', color=RED_E)
		error.move_to(TOP*0.8)
		double_edge = Ellipse(color=RED_E, stroke_width=10)
		double_edge.stretch(3,0)
		double_edge.move_to(UP/5)
		double_edge.rotate(-13*DEGREES)

		self.add(double_edge)
		self.add(error)
		self.add(g)

class MaxMinDegrees(Scene):
	def construct(self):
		nd = [
			{'name': 'A', 'pos': LEFT*2+UP*2},
			{'name': 'B', 'pos': LEFT*1.7+DOWN*2},
			{'name': 'C', 'pos': RIGHT*0.25+DOWN*0.25},
			{'name': 'D', 'pos': RIGHT*3+UP/2},
		]
		ei = [ (0,1),(0,2),(0,3),(1,2) ]
		color_info = { 'A': RED_E, 'D': GREEN_E}
		g = graph_gen(nd, ei, vertex_colors=color_info)

		mindeg = TexText('$\delta$ = 1', color=GREEN_E).move_to(RIGHT*3+DOWN/2)
		maxdeg = TexText('$\Delta$ = 3', color=RED_E).move_to(LEFT*3.5+UP*2)
		self.add(mindeg)
		self.add(maxdeg)
		self.add(g)

		# TODO: implement node coloring

class WeightedGraph(Scene):
	def construct(self):
		nd = [
			{'pos': LEFT*3+UP*2.5},
			{'pos': LEFT*2},
			{'pos': LEFT+DOWN*2},
			{'pos': RIGHT*2},
		]
		ei = [(0,1),(1,2),(0,3),(1,3),(2,3)]

		w1 = TexText('3', color=BLACK).move_to(LEFT*3+UP)
		w2 = TexText('1', color=BLACK).move_to(LEFT*2.2+DOWN*1.25)
		w3 = TexText('5', color=BLACK).move_to(LEFT*0.3+UP*1.7)
		w4 = TexText('9', color=BLACK).move_to(LEFT*0.2+UP*0.3)
		w5 = TexText('3', color=BLACK).move_to(RIGHT+DOWN*1.5)

		g = graph_gen(nd, ei, color=BLUE_A)

		self.add(w1)
		self.add(w2)
		self.add(w3)
		self.add(w4)
		self.add(w5)
		self.add(g)

class Forest(Scene):
	def construct(self):
		nd = [
			{ 'name': 1,'pos': UP*2+LEFT*3 },
			{ 'name': 2,'pos': UP*1+LEFT*2 },
			{ 'name': 3,'pos': UP*1+RIGHT*4 },
			{ 'name': 4,'pos': DOWN*0.2+LEFT*2 },
			{ 'name': 5,'pos': DOWN*0.2+RIGHT*2 },
			{ 'name': 6,'pos': DOWN*0.2+LEFT*0.8 },
			{ 'name': 7,'pos': DOWN*2+LEFT*2.8 },
			{ 'name': 8,'pos': DOWN*2+RIGHT*2.8 },
			{ 'name': 9,'pos': DOWN*2+LEFT },
			{ 'name': 10,'pos': DOWN*2+RIGHT },
		]

		ei = [ (0,1), (1,3), (1,5), (3,6), (3,8), (2,4), (4,7), (4,9) ]

		graph = graph_gen(nd, ei, color=GREEN_D)
		self.add(graph)

class Subgraphs(Scene):
	def construct(self):
		nd = [
			{'name': 'v1', 'pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'v2','pos': RIGHT*2.5+DOWN},
			{'name': 'v3','pos': LEFT*2.5+UP*3},
			{'name': 'v4','pos': LEFT*4},
			#{'name': 'v5','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			(0,2),(0,3),(1,2),(1,3),(2,3)#(1,4),q(3,4)
		]
		#ec={(0,3): RED, (0,2): RED, (2,3): RED, (1,2): RED, (1,3): RED }
		g = graph_gen(nd,ei)

		self.add(g)

class RegularGraph(Scene):
	def construct(self):
		nd = [
			{'pos': ORIGIN},
			{'pos': LEFT*2+UP*2},
			{'pos': LEFT*2+DOWN*3},
			{'pos': RIGHT*3+DOWN},
		]
		ei = [(0,1),(1,2),(2,3),(0,3)]
		graph = graph_gen(nd, ei)
		title = TexText('$\Delta$ = $\delta$ = 2', color=GREEN_E)
		title.move_to(RIGHT*3+UP*2)
		self.add(graph)
		self.add(title)

class NonRegularGraph(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2},
			{'name': 'b','pos': LEFT*2.5},
			{'name': 'c','pos': RIGHT*2.5},
			{'name': 'd','pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'e','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			(0,2),(0,3),(1,2),(1,3),(1,4),(2,3),(3,4)
		]

		graph = graph_gen(nd, ei, vertex_colors={'a': BLUE_E, 'd': GREEN_E})

		title = TexText('$\Delta$ $\\neq$ $\delta$', color=RED_E)
		degreea = Text('deg(a) = 2', color=BLUE_E)
		degreed = Text('deg(d) = 4', color=GREEN_E)
		title.move_to(RIGHT*3+UP*2)
		degreea.move_to(LEFT*2.5+UP*2)
		degreed.move_to(LEFT*3.5+DOWN*3)


		self.add(graph)
		self.add(title)
		self.add(degreea)
		self.add(degreed)

class CompleteGraphs(Scene):
	def construct(self):
		n = 10
		g = k_graph_gen(n, edge_colors={(0,1): RED})
		title = TexText('Grafo Completo K'+str(n), color=BLACK)
		title.move_to(TOP*0.8)
		g.move_to(DOWN/2)
		self.add(title)
		self.add(g)

class ComplementGraph(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2},
			{'name': 'b','pos': LEFT*2.5},
			{'name': 'c','pos': RIGHT*2.5},
			{'name': 'd','pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'e','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			#(0,2),(0,3),(1,2),(1,3),(1,4),(2,3),(3,4)
			(0,1),(0,4),(2,4)
		]

		graph = graph_gen(nd, ei, color=GOLD_C)

		self.add(graph)
class CompleteComplements(Scene):
	def construct(self):
		nd = [
			{'name': 'a','pos': UP*2},
			{'name': 'b','pos': LEFT*2.5},
			{'name': 'c','pos': RIGHT*2.5},
			{'name': 'd','pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'e','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			(0,2),(0,3),(1,2),(1,3),(1,4),(2,3),(3,4),(0,1),(0,4),(2,4)
		]
		ec = {
			(0,2): PURPLE_D,(0,3): PURPLE_D,(1,2): PURPLE_D,(1,3): PURPLE_D,(1,4): PURPLE_D,(2,3): PURPLE_D,(3,4): PURPLE_D,
			(0,1): GOLD_C, (0,4): GOLD_C, (2,4): GOLD_C
		}

		title = TexText('K5 subdividido em','\space G','\space e','\space $\overline{G}$', color=BLACK)
		title.set_color_by_tex_to_color_map({'G': PURPLE_E, '$\overline{G}$': GOLD_C})
		title.move_to(TOP*0.8)
		graph = graph_gen(nd, ei, color=TEAL_D, edge_colors=ec)

		self.add(title)
		self.add(graph)

class CompleteColoring(Scene):
	def construct(self):
		kgraph = k_graph_gen(10, colored=True)
		self.add(kgraph)

class StronglyConnected(Scene):
	def construct(self):
		nd = [
			{'name': 'A', 'pos': LEFT*2+UP*2},
			{'name': 'B', 'pos': LEFT*1.7+DOWN*2},
			{'name': 'C', 'pos': RIGHT*0.25+DOWN*0.25},
			{'name': 'D', 'pos': RIGHT*3+UP/2},
		]
		ei = [
			(0,1),(0,2),(0,3),(1,2)
		]
		g = graph_gen(nd,ei,direct=True)
		self.add(g)
		arrows = VGroup()
		arrows.add(CurvedArrow(LEFT*2.2+DOWN*2,LEFT*2.5+UP*2,color=GREY_E,width=8,angle=-PI*0.75).set_stroke(width=7))
		arrows.add(CurvedArrow(RIGHT*0.7+DOWN/2,LEFT*1.5+DOWN*2.5,color=GREY_E,width=8,angle=-PI).set_stroke(width=7))
		arrows.add(CurvedArrow(LEFT/4+DOWN*0.25,LEFT*1.8+UP*1.5,color=GREY_E,width=8,angle=-PI/3).set_stroke(width=7))
		arrows.add(CurvedArrow(RIGHT*2.9+UP,LEFT*2+UP*2.5,color=GREY_E,width=8,angle=PI*0.75).set_stroke(width=7))
		self.add(arrows)
class BridgeGraph(Scene):
	def construct(self):
		nd = [
			{'pos': LEFT*2+UP*3},
			{'pos': LEFT*2},
			{'pos': LEFT*2+DOWN*3},
			{'pos': RIGHT*2},
		]
		ei = [
			(0,1),(1,0),(1,2),(2,1),
			(3,0),(3,1),(3,2)
		]
		g = graph_gen(nd,ei,curved_edges=[(0,1),(1,0),(1,2),(2,1)], color=ORANGE)
		self.add(g)

class DirectedDegrees(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT/2+DOWN},
			{'name': 'v2','pos': LEFT*1.5+DOWN*2.5},
			{'name': 'v3','pos': RIGHT*1.2+DOWN*2.7},
			{'name': 'v4','pos': RIGHT*2+UP/3},
			{'name': 'v5','pos': RIGHT*2.2+UP*2.5},
			{'name': 'v6','pos': RIGHT/3+UP*1.3},
			{'name': 'v7','pos': LEFT*1.5+UP/2},
			{'name': 'v8','pos': LEFT*1.1+UP*2.7},
		]
		ei = [(0,1),(1,2),(2,0),(0,3),(3,4),(4,7),(5,4),(5,0),(7,5),(7,6),(6,0),(0,7)]
		g = graph_gen(nd,ei,size=0.75,color=ORANGE, direct=True, vertex_colors={'v1': BLUE, 'v6': GREEN}).move_to(LEFT*4)
		degv1 = VGroup(
			TexText('deg$^{+}$(v1) = 3',color=BLUE, background_color=BLACK).set_stroke(BLACK, 6, background=True),
			TexText('deg$^{-}$(v1) = 3',color=BLUE).set_stroke(BLACK, 6, background=True)
		).arrange(DOWN, buff=0.4).move_to(DOWN*1.3+LEFT*2.2)

		degv6 = VGroup(
			TexText('deg$^{+}$(v6) = 2',color=GREEN, background_color=BLACK).set_stroke(BLACK, 6, background=True),
			TexText('deg$^{-}$(v6) = 1',color=GREEN).set_stroke(BLACK, 6, background=True)
		).arrange(DOWN, buff=0.4).move_to(UP*1.2+LEFT/5)
		deltas = VGroup(
			TexText('$\\Delta^{+}$ = 3',color=RED, background_color=BLACK).set_stroke(BLACK, 6, background=True),
			TexText('$\\Delta^{-}$ = 3',color=RED, background_color=BLACK).set_stroke(BLACK, 6, background=True),
			TexText('$\\delta^{+}$ = 1',color=YELLOW, background_color=BLACK).set_stroke(BLACK, 6, background=True),
			TexText('$\\delta^{-}$ = 1',color=YELLOW, background_color=BLACK).set_stroke(BLACK, 6, background=True),
		).arrange(DOWN, buff=0.4).move_to(RIGHT*3+DOWN)
		self.add(VGroup(g,degv1,degv6, deltas))
#############################################################################################
#																							#
#										GIFS												#
#																							#
#############################################################################################
# and it's pronounced "gif", not "gif"
# (also, I had to render a video and convert it to a gif with ffmpeg, cuz for some reason the gif render was broken or something idk, weird corruption or smth)

class MazeToGraph(Scene):
	def construct(self):
		cell = Rectangle(10,6,color=BLACK)
		self.play(ShowCreation(cell))
		lines = [
			Line(start=LEFT*5+DOWN*2, end=ORIGIN+DOWN*2, color=BLACK),
			Line(start=LEFT*2.5+DOWN, end=LEFT*4+DOWN, color=BLACK),
			Line(start=LEFT*2.5+UP, end=LEFT*2.5+DOWN, color=BLACK),
			Line(start=LEFT*5+UP, end=LEFT*2.5+UP, color=BLACK),
			Line(start=LEFT*4, end=LEFT*4+DOWN, color=BLACK),
			Line(start=DOWN*3+RIGHT*1.2, end=RIGHT*1.2, color=BLACK),
			Line(start=UP*2+RIGHT*3, end=RIGHT*3+DOWN*2, color=BLACK),
			Line(start=UP*2+RIGHT*5, end=LEFT*4+UP*2, color=BLACK),
			Line(start=UP*2, end=ORIGIN, color=BLACK),
			Line(start=DOWN*2+LEFT, end=LEFT, color=BLACK),
		]

		walls = VGroup()

		for line in lines:
			walls.add(line)
		entrada = TexText('ENTRADA', color=GREEN_E)
		entrada.move_to(LEFT*5+DOWN*3.5)
		saida = TexText('SAÍDA', color=RED_E)
		saida.move_to(RIGHT*5+UP*3.5)

		maze_entrance = Circle(color=WHITE)
		maze_exit = Circle(color=WHITE)
		maze_entrance.set_fill(WHITE, opacity=1) 
		maze_exit.set_fill(WHITE, opacity=1)
		maze_entrance.scale(0.4)
		maze_exit.scale(0.4)
		maze_entrance.move_to(LEFT*5+DOWN*2.5)
		maze_exit.move_to(RIGHT*5+UP*2.5)
		walls.add(maze_exit)
		walls.add(maze_entrance)
		self.play(ShowCreation(walls))

		self.play(
			Transform(maze_exit, maze_exit.set_fill(RED_E)),
			Transform(maze_entrance, maze_entrance.set_fill(GREEN_E)),
			Write(saida),
			Write(entrada),
			run_time=0.3
		)
		self.wait(2)
		nd = [
			{'name': 1,'pos': LEFT*5+DOWN*2.5},
			{'name': 2, 'pos': DOWN*2+RIGHT/2},
			{'name': 3,'pos': DOWN*0.75},
			{'name': 4,'pos': RIGHT*1.2+UP},
			{'name': 5,'pos': RIGHT*3+DOWN*2.5},
			{'name': 6,'pos': RIGHT*4+UP},
			{'name': 7,'pos': LEFT+UP},
			{'name': 8,'pos': LEFT*3.3+DOWN/2},
			{'name': 9,'pos': LEFT*4.5+UP*2},
			{'name': 10,'pos': RIGHT*5+UP*2.5},
		]
		ei = [
			(0,1),(1,2),(2,3),(3,4),(4,5),(2,6),(6,7),(7,8),(8,9)
		]
		g = graph_gen(nd, ei, color=YELLOW_E, size=0.75)
		self.play(ShowCreation(g), run_time=3)
		self.wait()
		title = TexText('Grafo característico do labirinto', color=BLACK)
		title.move_to(TOP*0.8)
		self.play(
			Write(title),
			FadeOut(cell),
			FadeOut(walls),
			FadeOut(maze_exit),
			FadeOut(maze_entrance), 
			FadeOut(saida), 
			FadeOut(entrada), 
			Transform(g,graph_gen(nd, ei, color=YELLOW_E))
		)
		self.wait(3)

		self.play(FadeOut(title), FadeOut(g))

class MazeSolve(Scene):
	def construct(self):
		nd = [
			{'name': 1,'pos': LEFT*5+DOWN*2.5},
			{'name': 2, 'pos': DOWN*2+RIGHT/2},
			{'name': 3,'pos': DOWN*0.75},
			{'name': 4,'pos': RIGHT*1.2+UP},
			{'name': 5,'pos': RIGHT*3+DOWN*2.5},
			{'name': 6,'pos': RIGHT*4+UP},
			{'name': 7,'pos': LEFT+UP},
			{'name': 8,'pos': LEFT*3.3+DOWN/2},
			{'name': 9,'pos': LEFT*4.5+UP*2},
			{'name': 10,'pos': RIGHT*5+UP*2.5},
		]
		ei = [
			(0,1),(1,2),(2,3),(3,4),(4,5),(2,6),(6,7),(7,8),(8,9)
		]
		title = TexText('Resolução com algoritmo de busca em profundidade.',color=BLACK).move_to(TOP*0.8)
		g,vr = graph_gen(nd, ei, color=YELLOW_E, size=0.75,data=True,vertex_colors={'10': GREEN, '1': RED})
		g.move_to(DOWN)
		self.play(ShowCreation(g),run_time=3)
		self.play(Write(title))
		self.wait()
		dfs_path,found = vr[0].dfs(vr[9],vcolors='#0000FF',found_color='#FF0000',first_color='#00FF00',ecolors='#0000FF')
		dfs_path.move_to(DOWN)
		self.play(ShowCreation(dfs_path), run_time=20)
		self.wait()
		self.play(FadeOut(dfs_path), FadeOut(title))
		self.play(FadeOut(g))

class NonDirectedToDirected(Scene):
	def construct(self):
		nd = [
			{'name': 'A', 'pos': LEFT*2.5+DOWN*2},
			{'name': 'B', 'pos': UP*2},
			{'name': 'C', 'pos': RIGHT*2.5+DOWN*2},
		]
		ei = [ (0,1), (0,2), (1,2) ]
		g = graph_gen(nd, ei, color=RED_E)

		title = Text('Grafo não direcionado', color=BLACK, t2c={'não': RED_E})
		title.move_to(TOP*0.8)

		self.play(ShowCreation(g))
		self.play(Write(title))

		self.wait()

		self.play(Transform(title, Text('Grafo direcionado', color=BLACK).move_to(TOP*0.8)))
		self.play(Transform(g, graph_gen(nd, ei, color=BLUE_E, direct=True)))

		self.wait()

		self.play(Transform(title, Text('Grafo não direcionado', color=BLACK, t2c={'não': RED_E}).move_to(TOP*0.8)))
		self.play(Transform(g, graph_gen(nd, ei, color=RED_E)))
		
		self.wait()

		self.play(FadeOut(title), FadeOut(g))

class GraphLoop(Scene):
	def construct(self):
		nd = [
			{'name': 'v1', 'pos': LEFT*2.5+DOWN*2},
			{'name': 'v2', 'pos': UP*2},
			{'name': 'v3', 'pos': RIGHT*1.5+DOWN*1.5},
		]
		ei = [ (0,1), (0,2), (1,2) ]
		g = graph_gen(nd, ei, color=RED_E)

		# not an actual loop, I'll only have enough time to develop this properly after the due date i fear
		loop = Ellipse(color=BLACK, stroke_width=10, opacity=1)
		loop.move_to(LEFT*3.3+DOWN*2)
		t = TexText('Laço em v1', color=YELLOW_E).move_to(LEFT*3.3+DOWN)
		self.play(ShowCreation(loop),ShowCreation(g))
		self.play(
			Transform(loop, Ellipse(color=YELLOW_E, stroke_width=10, opacity=1).move_to(LEFT*3.3+DOWN*2)),
			Write(t)
		)
		self.wait(3)
		tip = ArrowTip(fill_color=YELLOW_E, angle=-10*DEGREES).move_to(LEFT*2.9+DOWN*1.5)
		self.play(
			Transform(g,graph_gen(nd, ei, color=BLUE_E, direct=True)),
			FadeIn(tip)
		)
		self.add(tip)
		self.wait(3)

		self.play(FadeOut(VGroup(g, loop, t, tip)))

class AdjacencyMatrix(Scene):
	def construct(self):
		nd = [
			{'name': 1,'pos': RIGHT*3},
			{'name': 2,'pos': RIGHT*1.8+DOWN*2},
			{'name': 3,'pos': LEFT*1.7 + DOWN*2},
			{'name': 4,'pos': LEFT*1.5 + UP},
			{'name': 5,'pos': RIGHT + UP*0.8},
			{'name': 6,'pos': LEFT*3.5 + UP*2}
		]
		ei = [
			(0,4),(0,1),(1,4),(1,2),(2,3),(3,4),(3,5),(5,3),
		]
		g = graph_gen(nd, ei)
		g.move_to(LEFT*3)
		grid = self.create_grid(len(nd), len(nd), 0.7)
		matrix = self.get_group_grid(grid)
		matrix.move_to(RIGHT*4)
		labels = TexText('1 \space\space 2 \space\space 3 \space\space 4 \space\space 5 \space\space 6', color=BLACK)
		labels_side = VGroup(
			TexText('1',color=BLACK),
			TexText('2',color=BLACK),
			TexText('3',color=BLACK),
			TexText('4',color=BLACK),
			TexText('5',color=BLACK),
			TexText('6',color=BLACK),
		)
		labels_side.arrange(DOWN, buff=0.4)
		labels_side.move_to(RIGHT*1.5)
		labels.move_to(RIGHT*4+UP*2.5)
		#matrix.set_color(BLACK)
		self.play(ShowCreation(g))
		self.play(ShowCreation(matrix))
		self.play(Write(labels))
		self.play(Write(labels_side))
		self.wait(2)
		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'1': GOLD_C}).move_to(LEFT*3)))

		self.wait()
		# self.play(
		# 	Transform(g, graph_gen(nd, ei, vertex_colors={'1': GOLD_C, '2': RED_D, '5': RED_D}).move_to(LEFT*3))
		# )
		self.play(
			Transform(g, graph_gen(nd, ei, 
				vertex_colors={'1': GOLD_C, '2': RED_D, '5': RED_D},
				edge_colors={(0,1): RED_D, (0,4): RED_D}
			).move_to(LEFT*3))
		)
		column_1 = VGroup(
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
		)
		column_1.arrange(DOWN, buff=0.4).move_to(RIGHT*2.2)
		row_1 = VGroup(
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
		)
		row_1.arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*4.3+UP*1.8)
		self.play(Write(column_1))
		self.play(Write(row_1))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)))
		self.play(
			Transform(
				row_1, VGroup(
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*4.3+UP*1.8)
			),
			Transform(
				column_1, VGroup(
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(DOWN, buff=0.4).move_to(RIGHT*2.2)
			)
		)
		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'2': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(
			g, graph_gen(
				nd, ei, vertex_colors={'2': GOLD_C,'1': RED_D, '5': RED_D, '3': RED_D},
				edge_colors={(0,1): RED_D, (1,4): RED_D, (1,2): RED_D}
			).move_to(LEFT*3)
		))
		column_2 = VGroup(
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
		)
		column_2.arrange(DOWN, buff=0.4).move_to(RIGHT*2.9+DOWN*0.35)
		row_2 = VGroup(
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
		)
		row_2.arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*4.65+UP*1.1)
		self.play(Write(column_2))
		self.play(Write(row_2))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)))
		self.play(
			Transform(
				row_2, VGroup(
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*4.65+UP*1.1)
			),
			Transform(
				column_2, VGroup(
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(DOWN, buff=0.4).move_to(RIGHT*2.9+DOWN*0.35)
			)
		)

		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'3': GOLD_C}).move_to(LEFT*3)))
		self.play(Transform(
			g, graph_gen(
				nd, ei, vertex_colors={'3': GOLD_C,'2': RED_D, '4': RED_D},
				edge_colors={(1,2): RED_D, (2,3): RED_D}
			).move_to(LEFT*3)
		))
		column_3 = VGroup(
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('0',color=BLACK),
		)
		column_3.arrange(DOWN, buff=0.4).move_to(RIGHT*3.6+DOWN*0.7)
		row_3 = VGroup(
			TexText('1',color=RED_D),
			TexText('0',color=BLACK),
			TexText('0',color=BLACK),
		)
		row_3.arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*5+UP*0.4)
		self.play(Write(column_3))
		self.play(Write(row_3))
		self.wait()
		self.play(
			Transform(
				row_3, VGroup(
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*5+UP*0.4)
			),
			Transform(
				column_3, VGroup(
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('0',color=BLACK),
						TexText('0',color=BLACK),
					).arrange(DOWN, buff=0.4).move_to(RIGHT*3.6+DOWN*0.7)
			)
		)
		
		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'4': GOLD_C}).move_to(LEFT*3)))
		self.play(Transform(
			g, graph_gen(
				nd, ei, vertex_colors={'4': GOLD_C,'6': RED_D, '5': RED_D},
				edge_colors={(3,5): RED_D, (3,4): RED_D, (5,3): RED_D}
			).move_to(LEFT*3)
		))
		column_4 = VGroup(
			TexText('0',color=BLACK),
			TexText('1',color=RED_D),
			TexText('2',color=RED_D),
		)
		column_4.arrange(DOWN, buff=0.4).move_to(RIGHT*4.3+DOWN*1.05)
		row_4 = VGroup(
			TexText('1',color=RED_D),
			TexText('2',color=RED_D),
		)
		row_4.arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*5.35+DOWN/3)
		self.play(Write(column_4))
		self.play(Write(row_4))
		self.wait()
		self.play(
			Transform(
				row_4, VGroup(
						TexText('1',color=BLACK),
						TexText('2',color=BLACK),
					).arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*5.35+DOWN/3)
			),
			Transform(
				column_4, VGroup(
						TexText('0',color=BLACK),
						TexText('1',color=BLACK),
						TexText('2',color=BLACK),
					).arrange(DOWN, buff=0.4).move_to(RIGHT*4.3+DOWN*1.05)
			)
		)

		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'5': GOLD_C}).move_to(LEFT*3)))
		self.play(Transform(
			g, graph_gen(
				nd, ei, vertex_colors={'5': GOLD_C,'4': RED_D, '1': RED_D, '2': RED_D},
				edge_colors={(0,4): RED_D, (1,4): RED_D, (3,4): RED_D}
			).move_to(LEFT*3)
		))
		column_5 = VGroup(
			TexText('0',color=BLACK),
			TexText('0',color=BLACK),
		)
		column_5.arrange(DOWN, buff=0.4).move_to(RIGHT*5+DOWN*1.4)
		row_5 = VGroup(
			TexText('0',color=BLACK),
		)
		row_5.arrange(RIGHT, buff=MED_LARGE_BUFF).move_to(RIGHT*5.7+DOWN*1.025)
		self.play(Write(column_5))
		self.play(Write(row_5))
		self.wait()

		self.play(Transform(g, graph_gen(nd, ei, vertex_colors={'6': GOLD_C}).move_to(LEFT*3)))
		self.play(Transform(
			g, graph_gen(
				nd, ei, vertex_colors={'6': GOLD_C,'4': RED_D},
				edge_colors={(3,5): RED_D, (5,3): RED_D}
			).move_to(LEFT*3)
		))

		column_6 = TexText('0', color=BLACK).move_to(RIGHT*5.7+DOWN*1.75)

		self.play(Write(column_6))
		self.play(Transform(g, graph_gen(nd, ei).move_to(LEFT*3)))
		self.wait()
		self.play(FadeOut(g))
		table = VGroup(matrix,
		column_1,column_2,column_3,column_4,column_5,column_6,
		row_1,row_2,row_3,row_4,row_5,labels, labels_side
		)
		self.play(table.animate.move_to(ORIGIN))
		name = TexText('Matriz de Adjacências de G', color=BLACK).move_to(TOP*0.8)
		self.play(Write(name))
		self.wait(3)
		self.play(FadeOut(table), FadeOut(name))
		

	def create_grid(self, rows, columns, square_length):
		left_corner = Square(side_length=square_length, color=BLACK)
		grid = []
		first_row = [left_corner]
		for i in range(columns - 1):
			square = Square(side_length=square_length, color=BLACK)
			square.next_to(first_row[i], RIGHT, buff=0)
			first_row.append(square)
		grid.append(first_row)
		for i in range(rows - 1):
			prev_row = grid[i]
			new_row = []
			for square in prev_row:
				square_below = Square(side_length=square_length, color=BLACK)
				square_below.next_to(square, DOWN, buff=0)
				new_row.append(square_below)
			grid.append(new_row)

		return grid
	def get_group_grid(self, grid):
		squares = []
		for row in grid:
			for square in row:
				squares.append(square)
		return VGroup(*squares)

class AdjacencyList(Scene):
	def construct(self):
		nd = [
			{'name': 1,'pos': RIGHT*3},
			{'name': 2,'pos': RIGHT*1.8+DOWN*2},
			{'name': 3,'pos': LEFT*1.7 + DOWN*2},
			{'name': 4,'pos': LEFT*1.5 + UP},
			{'name': 5,'pos': RIGHT + UP*0.8},
			{'name': 6,'pos': LEFT*3.5 + UP*2}
		]
		ei = [
			(0,4),(0,1),(1,4),(1,2),(2,3),(3,4),(3,5),(5,3),
		]
		g = graph_gen(nd, ei)
		g.move_to(LEFT*3)
		grid = self.create_grid(len(nd),1,0.7)
		adj_list = self.get_group_grid(grid).move_to(RIGHT*2)
		self.play(ShowCreation(g), ShowCreation(adj_list))

		lists = VGroup()
		for i in range(len(nd)):
			lists.add(TexText(str(i+1), color=BLACK))
		lists.arrange(DOWN, buff=0.4).move_to(RIGHT*2)
		self.play(Write(lists))
		adjs = []
		grids = []
		############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'1': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'1': GOLD_C, '2': RED_C, '5': RED_C}, edge_colors={(0,1): RED_C, (0,4): RED_C}
		).move_to(LEFT*3)))
		self.wait()
		#
		adjs.append(VGroup(
			TexText('2', color=RED_C), 
			TexText('5', color=RED_C)
		))
		adjs[0].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+UP*1.75)
		grids.append(self.get_group_grid(self.create_grid(1,2,0.7)))
		grids[0].move_to(RIGHT*3.2+UP*1.75)
		self.play(ShowCreation(grids[0]), Write(adjs[0]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[0], VGroup(
			TexText('2', color=BLACK), 
			TexText('5', color=BLACK)
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+UP*1.75)))
		###############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'2': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'2': GOLD_C,'1': RED_C, '3': RED_C, '5': RED_C}, edge_colors={(0,1): RED_C,(1,2): RED_C, (1,4): RED_C}
		).move_to(LEFT*3)))
		adjs.append(VGroup(
			TexText('1', color=RED_C), 
			TexText('3', color=RED_C), 
			TexText('5', color=RED_C)
		))
		adjs[1].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.55+UP*1.05)
		grids.append(self.get_group_grid(self.create_grid(1,3,0.7)))
		grids[1].move_to(RIGHT*3.55+UP*1.05)
		self.play(ShowCreation(grids[1]), Write(adjs[1]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[1], VGroup(
			TexText('1', color=BLACK), 
			TexText('3', color=BLACK), 
			TexText('5', color=BLACK)
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.55+UP*1.05)))
		###############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'3': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'3': GOLD_C, '2': RED_C, '4': RED_C}, edge_colors={(1,2): RED_C, (2,3): RED_C}
		).move_to(LEFT*3)))
		adjs.append(VGroup(
			TexText('2', color=RED_C), 
			TexText('4', color=RED_C), 
		))
		adjs[2].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+UP*0.35)
		grids.append(self.get_group_grid(self.create_grid(1,2,0.7)))
		grids[2].move_to(RIGHT*3.2+UP*0.35)
		self.play(ShowCreation(grids[2]), Write(adjs[2]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[2], VGroup(
			TexText('2', color=BLACK), 
			TexText('4', color=BLACK)
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+UP*0.35)))
		###############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'4': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'4': GOLD_C, '3': RED_C, '5': RED_C, '6': RED_C}, 
			edge_colors={(2,3): RED_C, (3,4): RED_C, (3,5): RED_C, (5,3): RED_C}
		).move_to(LEFT*3)))
		adjs.append(VGroup(
			TexText('3', color=RED_C), 
			TexText('5', color=RED_C), 
			TexText('6', color=RED_C), 
			TexText('6', color=RED_C), 
		))
		adjs[3].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.9+DOWN*0.35)
		grids.append(self.get_group_grid(self.create_grid(1,4,0.7)))
		grids[3].move_to(RIGHT*3.9+DOWN*0.35)
		self.play(ShowCreation(grids[3]), Write(adjs[3]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[3], VGroup(
			TexText('3', color=BLACK), 
			TexText('5', color=BLACK),
			TexText('6', color=BLACK),
			TexText('6', color=BLACK),
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.9+DOWN*0.35)))
		###############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'4': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'5': GOLD_C, '1': RED_C, '2': RED_C, '4': RED_C}, 
			edge_colors={(3,4): RED_C, (0,4): RED_C, (1,4): RED_C}
		).move_to(LEFT*3)))
		adjs.append(VGroup(
			TexText('1', color=RED_C), 
			TexText('2', color=RED_C), 
			TexText('4', color=RED_C), 
		))
		adjs[4].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.55+DOWN*1.05)
		grids.append(self.get_group_grid(self.create_grid(1,3,0.7)))
		grids[4].move_to(RIGHT*3.55+DOWN*1.05)
		self.play(ShowCreation(grids[4]), Write(adjs[4]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[4], VGroup(
			TexText('1', color=BLACK), 
			TexText('2', color=BLACK),
			TexText('4', color=BLACK),
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.55+DOWN*1.05)))
		###############
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'6': GOLD_C}).move_to(LEFT*3)))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei,
			vertex_colors={'6': GOLD_C, '4': RED_C}, 
			edge_colors={(5,3): RED_C, (3,5): RED_C}
		).move_to(LEFT*3)))
		adjs.append(VGroup(
			TexText('4', color=RED_C), 
			TexText('4', color=RED_C), 
		))
		adjs[5].arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+DOWN*1.75)
		grids.append(self.get_group_grid(self.create_grid(1,2,0.7)))
		grids[5].move_to(RIGHT*3.2+DOWN*1.75)
		self.play(ShowCreation(grids[5]), Write(adjs[5]))
		self.wait()
		self.play(Transform(g, graph_gen(nd,ei).move_to(LEFT*3)), Transform(adjs[5], VGroup(
			TexText('4', color=BLACK),
			TexText('4', color=BLACK),
		).arrange(RIGHT, buff=0.5).move_to(RIGHT*3.2+DOWN*1.75)))

		whole_info = VGroup(adj_list, lists)
		for adj in adjs:
			whole_info.add(adj)
		for gd in grids:
			whole_info.add(gd)
		self.wait(2)
		title = TexText('Lista de Adjacências de G', color=BLACK).move_to(TOP*0.8)
		self.play(FadeOut(g))
		self.play(whole_info.animate.move_to(ORIGIN), Write(title))
		self.wait(3)
		self.play(FadeOut(VGroup(title, whole_info)))

	def create_grid(self, rows, columns, square_length):
		left_corner = Square(side_length=square_length, color=BLACK)
		grid = []
		first_row = [left_corner]
		for i in range(columns - 1):
			square = Square(side_length=square_length, color=BLACK)
			square.next_to(first_row[i], RIGHT, buff=0)
			first_row.append(square)
		grid.append(first_row)
		for i in range(rows - 1):
			prev_row = grid[i]
			new_row = []
			for square in prev_row:
				square_below = Square(side_length=square_length, color=BLACK)
				square_below.next_to(square, DOWN, buff=0)
				new_row.append(square_below)
			grid.append(new_row)

		return grid
	def get_group_grid(self, grid):
		squares = []
		for row in grid:
			for square in row:
				squares.append(square)
		return VGroup(*squares)

class EdgeSet(Scene):
	def construct(self):
		nd = [
			{'name': 1,'pos': RIGHT*3},
			{'name': 2,'pos': RIGHT*1.8+DOWN*2},
			{'name': 3,'pos': LEFT*1.7 + DOWN*2},
			{'name': 4,'pos': LEFT*1.5 + UP},
			{'name': 5,'pos': RIGHT + UP*0.8},
			{'name': 6,'pos': LEFT*3.5 + UP*2}
		]
		ei = [
			(0,1),(0,4),(1,2),(1,4),(2,3),(3,4),(3,5),(5,3),
		]
		g = graph_gen(nd, ei, direct=True)
		self.wait()
		edge_set = Tex('\\tiny E = \{\}', color=BLACK)
		self.play(Write(edge_set))
		posis = [RIGHT,UP,UP,LEFT,RIGHT,UP,UP,DOWN]
		for edge_number in range(0,len(ei)):
			text = '\\tiny {E = \{'
			for i in range(0,edge_number):
				text = text + '('+str(ei[i][0]+1) +','+ str(ei[i][1]+1)+')'
				if i < edge_number-1:
					text = text+','
			text = text + '\}}'
			target = Tex(text, color=BLACK).move_to(BOTTOM*0.8)
			if edge_number != 0:
				edg = Tex('('+str(ei[edge_number-1][0]+1)+','+str(ei[edge_number-1][1]+1)+')', color=BLACK)
				pos = (nd[ei[edge_number-1][0]]['pos']+nd[ei[edge_number-1][1]]['pos'])/2+posis[edge_number-1]
				edg.move_to(pos)
				self.play(Write(edg))
				self.wait()
				self.play(FadeOut(edg))
			self.play(TransformMatchingShapes(edge_set, target, path_arc=PI))
			edge_set=target
			if edge_number == 0:
				self.play(ShowCreation(g))
			else:
				self.play(Transform(g, graph_gen(nd, ei, direct=True)))
			self.wait()
			self.play(Transform(g, graph_gen(nd, ei, direct=True, edge_colors={ei[edge_number]: RED_C})))
			self.wait()
			if edge_number == len(ei)-1:
				edg = Tex('\{'+str(ei[edge_number][0]+1)+','+str(ei[edge_number][1]+1)+'\}', color=BLACK)
				pos = (nd[ei[edge_number][0]]['pos']+nd[ei[edge_number][1]]['pos'])/2+posis[edge_number]
				edg.move_to(pos)
				self.play(Write(edg))
				self.wait()
				self.play(FadeOut(edg))
				self.play(TransformMatchingShapes(edge_set, target, path_arc=PI))
				edge_set=target
				self.play(Transform(g, graph_gen(nd, ei, direct=True)))
				self.play(FadeOut(target))
		
		title = TexText('Conjunto de arestas de G direcionado', color=BLACK).move_to(TOP*0.8)
		self.play(FadeOut(g), edge_set.animate.move_to(ORIGIN), Write(title))
		self.wait(3)
		self.play(FadeOut(edge_set), FadeOut(title))
	
class GraphKmn(Scene):
	def construct(self):
		nd = [
			{'name': 'a1', 'pos': LEFT*2+UP},
			{'name': 'a2', 'pos': LEFT*2+DOWN},
			{'name': 'b1', 'pos': RIGHT*2+UP*2},
			{'name': 'b2', 'pos': RIGHT*2},
			{'name': 'b3', 'pos': RIGHT*2+DOWN*2},
		]
		ei = [
			(0,2),(0,3),(0,4),(1,2),(1,3),(1,4)
		]
		g = graph_gen(nd, ei)
		
		line = Line(TOP, BOTTOM, color=RED_D)
		name = TexText('Grafo K2,3', color=BLACK).move_to(TOP*0.8+LEFT*0.85)
		self.play(ShowCreation(g), Write(name), run_time=3)
		self.wait()
		self.play(ShowCreation(line), run_time=2)
		self.wait(3)
		self.play(FadeOut(g), FadeOut(line), FadeOut(name))

class BipartiteGraph(Scene):
	def construct(self):
		nd = [
			{'pos': LEFT*4+DOWN},
			{'pos': UP*2},
			{'pos': RIGHT*5+UP*3},
			{'pos': LEFT+DOWN*3},
			{'pos': RIGHT*2+DOWN*2},
		]
		bp_nd = [
			{'pos': RIGHT*2+UP},
			{'pos': RIGHT*2+DOWN},
			{'pos': LEFT*2+UP*2},
			{'pos': LEFT*2},
			{'pos': LEFT*2+DOWN*2},
		]
		ei = [
			(0,2),(0,3),(0,4),(2,0),(1,2),(1,3),(1,4),(4,1)
		]
		g = graph_gen(nd, ei)
		
		line = Line(TOP, BOTTOM, color=RED_D)
		name = TexText('Grafo em forma bipartida', color=BLACK).move_to(TOP*0.8+LEFT*3)
		self.play(ShowCreation(g), run_time=2)
		self.wait(2)
		self.play(Transform(g, graph_gen(bp_nd, ei, color=GOLD_D)), run_time=3)
		self.wait()
		self.play( Write(name), ShowCreation(line))
		self.wait(3)
		self.play(FadeOut(VGroup(line,name,g)))
class PlanarityOfK2n(Scene):
	def construct(self):
		nd = [
			{'name': 'a1', 'pos': LEFT*2.7+UP*2},
			{'name': 'a2', 'pos': LEFT*2+DOWN},
			{'name': 'b1', 'pos': LEFT*4},
			{'name': 'b2', 'pos': RIGHT*2/5+UP*3},
			{'name': 'b3', 'pos': RIGHT*2+UP*2.2},
			{'name': 'b4', 'pos': RIGHT*5},
			{'name': 'b5', 'pos': RIGHT*2.3+DOWN*1.6},
			{'name': 'b6', 'pos': DOWN*2.4},
			{'name': 'bn', 'pos': LEFT*5+UP*3.3},
		]
		nd_transformed = [
			{'name': 'a1', 'pos': LEFT*2},
			{'name': 'a2', 'pos': RIGHT*2},
			{'name': 'b1', 'pos': UP*3},
			{'name': 'b2', 'pos': UP*2},
			{'name': 'b3', 'pos': UP},
			{'name': 'b4', 'pos': ORIGIN},
			{'name': 'b5', 'pos': DOWN},
			{'name': 'b6', 'pos': DOWN*2},
			{'name': 'bn', 'pos': DOWN*3},
		]
		ei = [
			(0,2),(0,3),(0,4),(0,5),(0,6),(0,7),(0,8),
			(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8)
		]
		g = graph_gen(nd, ei)
		
		self.play(ShowCreation(g), run_time=2)
		self.wait(6)
		self.play(Transform(g, graph_gen(nd_transformed, ei)), run_time=3)
		self.wait(3)
		self.play(FadeOut(g))

class K33NotPlanar(Scene):
	def construct(self):
		nd = [
			{'name': 'a1', 'pos': LEFT*2+UP*2},
			{'name': 'a2', 'pos': LEFT*2},
			{'name': 'a2', 'pos': LEFT*2+DOWN*2},
			{'name': 'b1', 'pos': RIGHT*2+UP*2},
			{'name': 'b2', 'pos': RIGHT*2},
			{'name': 'b3', 'pos': RIGHT*2+DOWN*2},
		]
		ei = [
			(0,3),(0,4),(0,5),(1,3),(1,4),(1,5),(2,3),(2,4),(2,5)
		]
		g = graph_gen(nd, ei)
		name = TexText('Grafo K3,3 (impossível planarizar)', color=BLACK).move_to(TOP*0.8)
		self.play(ShowCreation(g), Write(name), run_time=2)
		self.wait(3)
		self.play(FadeOut(VGroup(g,name)))

class K5NotPlanar(Scene):
	def construct(self):
		k5 = k_graph_gen(5)
		name = TexText('Grafo K5 (impossível planarizar)', color=BLACK).move_to(TOP*0.8)
		self.play(ShowCreation(k5), Write(name), run_time=2)
		self.wait(3)
		self.play(FadeOut(VGroup(k5,name)))
class K4Planar(Scene):
	def construct(self):
		k4 = k_graph_gen(4)
		self.play(ShowCreation(k4), run_time=2)
		self.wait(3)
		question = TexText('Não é planar? E se o grafo for rearranjado?', color=BLACK).move_to(TOP*0.8)
		self.play(FadeIn(question, shift=DOWN))
		self.wait(2)
		nd = [
			{'name': '1', 'pos': RIGHT*2},
			{'name': '2', 'pos': UP*2},
			{'name': '3', 'pos': LEFT*2},
			{'name': '4', 'pos': DOWN*2},
		]
		ei = [(0,1),(0,3),(1,2),(1,3),(2,3)]
		self.play(Transform(k4, graph_gen(nd, ei)))
		arc = ArcBetweenPoints(RIGHT*2+DOWN/2, LEFT*2+DOWN/2, angle=-PI*1.2, color=GREY_E)
		arc.set_stroke(width=7)
		self.play(ShowCreation(arc), FadeOut(question, shift=UP))
		self.wait(3)
		self.play(FadeOut(VGroup(k4, arc)))
class ProperColoring(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2},
			{'name': 'b','pos': LEFT*2.5},
			{'name': 'c','pos': RIGHT*2.5},
			{'name': 'd','pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'e','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			(0,2),(1,2),(1,3),(2,3),(3,4)
		]
		g = graph_gen(nd,ei)
		self.play(ShowCreation(g))
		self.wait()
		title = TexText('Coloração Própria de Grafo', color=BLACK).move_to(TOP*0.8)
		self.play(Write(title))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GOLD_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GOLD_C, 'b': RED_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GOLD_C, 'b': RED_C, 'c': GREEN_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GOLD_C, 'b': RED_C, 'c': GREEN_C, 'a':RED_C})))
		self.wait(3)
		self.play(FadeOut(VGroup(g,title)))

class ImproperColoring(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2},
			{'name': 'b','pos': LEFT*2.5},
			{'name': 'c','pos': RIGHT*2.5},
			{'name': 'd','pos': LEFT*1.3 + DOWN*2.5},
			{'name': 'e','pos': RIGHT*1.3 + DOWN*2.5}
		]
		ei = [
			(0,2),(1,2),(1,3),(2,3),(3,4)
		]
		g = graph_gen(nd,ei)
		self.play(ShowCreation(g))
		self.wait()
		title = TexText('Coloração Imprópria de Grafo', color=BLACK).move_to(TOP*0.8)
		self.play(Write(title))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C, 'b': RED_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C, 'b': RED_C, 'c': GREEN_C})))
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C, 'b': RED_C, 'c': GREEN_C, 'a':RED_C})))
		self.wait(3)
		error = TexText('Vértices Adjacentes com a mesma cor!', font_size=32, color=BLACK).move_to(RIGHT*3.3+DOWN*1.5)
		self.play(Write(error))
		for i in range(0,13):
			self.play(
				Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C, 'b': RED_C, 'c': GREEN_C, 'a':RED_C}, edge_colors={(2,3): RED_E})),
				Transform(error, TexText('Vértices Adjacentes com a mesma cor!', font_size=32, color=RED_E).move_to(RIGHT*3.3+DOWN*1.5)), run_time=0.3
			)
			self.play(
				Transform(g, graph_gen(nd,ei,vertex_colors={'e': RED_C, 'd':GREEN_C, 'b': RED_C, 'c': GREEN_C, 'a':RED_C})),
				Transform(error, TexText('Vértices Adjacentes com a mesma cor!', font_size=32, color=BLACK).move_to(RIGHT*3.3+DOWN*1.5)), run_time=0.3
			)

		self.play(FadeOut(VGroup(g,title,error)))

class Isomorphism(Scene):
	def construct(self):
		nd = [
			{'name': 'A', 'pos': LEFT*2+UP*2},
			{'name': 'B', 'pos': LEFT*1.7+DOWN*2},
			{'name': 'C', 'pos': RIGHT*0.25+DOWN*0.25},
			{'name': 'D', 'pos': RIGHT*3+UP/2},
		]
		ei = [ (0,1),(0,2),(0,3),(1,2) ]
		nd2 = [
			{'name': 'v1', 'pos': DOWN*3+RIGHT*4},
			{'name': 'v2', 'pos': RIGHT*2+UP*3},
			{'name': 'v3', 'pos': LEFT*3+DOWN},
			{'name': 'v4', 'pos': LEFT+UP*2},
		]
		g1 = graph_gen(nd,ei,color=GREEN_C).move_to(LEFT*3)
		g2 = graph_gen(nd2,ei,color=BLUE_C).move_to(RIGHT*3)
		self.play(ShowCreation(g1))
		self.play(ShowCreation(g2))
		self.wait(3)
		for i,v in enumerate(nd):
			nd2[i]['pos'] = v['pos']
		self.play(Transform(g2, graph_gen(nd2,ei,color=BLUE_C).move_to(RIGHT*3.5)), run_time=5)
		self.wait(3)
		self.play(FadeOut(VGroup(g1, g2)))

class WeaklyConnected(Scene):
	def construct(self):
		nd = [
			{'name': 'A', 'pos': LEFT*2+UP*2},
			{'name': 'B', 'pos': LEFT*1.7+DOWN*2},
			{'name': 'C', 'pos': RIGHT*0.25+DOWN*0.25},
			{'name': 'D', 'pos': RIGHT*3+UP/2},
		]
		ei = [ (0,1),(0,2),(0,3),(1,2) ]
		g = graph_gen(nd,ei,direct=True).move_to(DOWN)
		self.play(ShowCreation(g))
		self.wait(2)
		text = TexText('Versão não direcionada é conexa:', color=BLACK).move_to(TOP*0.8)
		subtext = TexText('grafo direcionado original é fracamente conexo!', color=PURPLE_E).move_to(TOP*0.6)
		self.play(Transform(g, graph_gen(nd,ei,color=TEAL_D).move_to(DOWN)), FadeIn(text, shift=DOWN), run_time=2)
		self.play(Write(subtext))
		self.wait(3)
		self.play(FadeOut(VGroup(g,text,subtext)))

class GraphSubdivision(Scene):
	def construct(self):
		nd = [
			{'name': 1, 'pos': UP*2},
			{'name': 2, 'pos': LEFT*3+UP},
			{'name': 3, 'pos': RIGHT*3+DOWN*1.6},
			{'name': 4, 'pos': RIGHT/2+DOWN*3},
		]
		new_node = {'name': 'v', 'pos': (nd[1]['pos']+nd[2]['pos'])/2}
		ei = [(0,1),(0,2),(3,2),(3,1),(1,2)]
		g = graph_gen(nd, ei)
		nd.append(new_node)
		ei = [(0,1),(0,2),(3,2),(3,1),(1,4),(4,2)]
		self.play(ShowCreation(g))
		self.wait(3)
		g2 = graph_gen(nd, ei,vertex_colors={'v': RED_E})
		t = TexText('Grafos homomórficos: um é subdivisão do outro',color=BLACK).move_to(TOP*0.8)
		self.play(FadeIn(g2))
		self.play(Write(t))
		self.play(g.animate.move_to(LEFT*3+DOWN*0.9), g2.animate.move_to(RIGHT*3+DOWN*0.9))
		self.wait(3)
		self.play(FadeOut(VGroup(g,g2,t)))
class CompleteCombination(Scene):
	def construct(self):
		n = 10
		g = k_graph_gen(n)
		lines = VGroup(
			TexText('\[ \\binom{n}{2} \]', color=BLACK).move_to(TOP*0.8+LEFT*3)
		)

		self.play(ShowCreation(g), Write(lines[0]), run_time=2)
		self.wait()
		for i in range(1,n+1):
			self.play(
				Transform(g, k_graph_gen(n,vertex_colors={str(i): RED_C})),
				Transform(lines[0], TexText('\[ \\binom{'+str(i)+'}{2} \]', color=BLACK).move_to(TOP*0.8+LEFT*3))
			)

		self.play(Transform(g, k_graph_gen(n)))
		self.play(FadeOut(g, shift=BOTTOM))
		opt = Tex('{',*self.fact_str(n),' \over ','2','.',*self.fact_str(n-2),'}', color=BLACK)
		lines.add(
			Tex('{',str(n),'!',' \over ','2','!','(',str(n),'-','2',')!}', color=BLACK),
			Tex('{',*self.fact_str(n),' \over ','2','(',str(n-2),')!}', color=BLACK),
			opt,
			Tex('{',str(self.fact(n)),' \over ',str(2*self.fact(n-2)),'}', color=BLACK),
		)
		if n-2<=1:
			lines.remove(opt)
		for i in range(1,len(lines)):
			lines[i].set_color_by_tex_to_color_map({'n': BLUE,'k': GREEN, str(n): BLUE, '2': GREEN, str(self.fact(n)): BLUE})
		lines.add(Tex('{',str(int(self.fact(n)/(2*self.fact(n-2)))),'}', color=BLACK),)
		lines.arrange(DOWN, buff=MED_SMALL_BUFF)
		keymaps = {
            str(n)+"!": str(self.fact(n)),
            "2!": "2",
			"("+str(n)+"-2)!": str(n-2),
			str(self.fact(n)): str(self.fact(n)/2),
        }
		for i in range(1,len(lines)):
			self.play(TransformMatchingTex(lines[i-1].copy(), lines[i], path_arc=PI, key_map=keymaps))
			self.wait()
		answer = TexText(str(int(self.fact(n)/(2*self.fact(n-2)))), color=BLACK).move_to(LEFT*4)
		linen = 4 if n<=3 else 5
		linecp = lines[linen]
		lines.remove(lines[linen])
		self.play(TransformMatchingTex(linecp, answer, path_arc=PI))
		self.play(FadeOut(lines, shift=RIGHT))
		self.wait()
		g.move_to(RIGHT)
		self.play(FadeIn(g, shift=UP))
		c = 0
		counter = TexText(str(c), color=BLACK).move_to(RIGHT*5)
		self.play(Write(counter))
		for i in range(0,int(self.fact(n)/(2*self.fact(n-2)))):
			self.play(
				Transform(g[1][i], g[1][i].set_stroke(color='#FF0000', width=14),run_time=0.1),
				Transform(counter, TexText(str(i+1), color=BLACK).move_to(RIGHT*5), run_time=0.1)
			)
		self.wait(3)
		self.play(FadeOut(VGroup(g,counter,answer)))
	def fact_str(self, n):
		string = [str(n)]
		if n > 1:
			string.append('.')
		for i in range(n-1, 1, -1):
			string.append(str(i))
			if i > 2:
				string.append('.')
		return string
	def fact(self, n):
		f = n
		for i in range(n-1, 1, -1):
			f*=i
		return f

class GraphWalk(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+DOWN},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*5+UP*3},
			{'name': 'v4','pos': LEFT+DOWN*3},
			{'name': 'v5','pos': RIGHT*2+DOWN*2},
		]
		ei = [
			(0,2),(0,3),(0,4),(2,0),(1,2),(1,3),(1,4),(4,1)
		]

		title = TexText('Caminho: maneira de percorrer um grafo de um vértice a outro',color=BLACK)
		self.play(Write(title))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho w = v2 $\\to$ v4 $\\to$ v1 $\\to$ v3',color=BLACK)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v4 $\\to$ v1 $\\to$ v3',color=BLACK).move_to(TOP*0.8+LEFT*4)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [1,3,0,2]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00')
		self.play(ShowCreation(walk),run_time=10)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk), FadeOut(title))
		self.wait()
		self.play(FadeOut(g))

class ClosedWalk(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+DOWN},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*5+UP*3},
			{'name': 'v4','pos': LEFT+DOWN*3},
			{'name': 'v5','pos': RIGHT*2+DOWN*2},
		]
		ei = [
			(0,2),(0,3),(0,4),(2,0),(1,2),(1,3),(1,4),(4,1)
		]
		
		title = TexText('Caminho fechado: ponto inicial é também o final',color=BLACK)
		self.play(Write(title))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho w = v3 $\\to$ v2 $\\to$ v5 $\\to$ v1 $\\to$ v3',color=BLACK)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v3 $\\to$ v2 $\\to$ v5 $\\to$ v1 $\\to$ v3',color=BLACK).move_to(TOP*0.8+LEFT*4)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [2,1,4,0,2]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00',last_color='#0000FF')
		self.play(ShowCreation(walk),run_time=10)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))

class SimpleWalk(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+DOWN},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*5+UP*3},
			{'name': 'v4','pos': LEFT+DOWN*3},
			{'name': 'v5','pos': RIGHT*2+DOWN*2},
		]
		ei = [
			(0,2),(0,3),(0,4),(2,0),(1,2),(1,3),(1,4),(4,1)
		]
		
		title = TexText('Caminho simples: caminho sem vértices repetidos',color=BLACK)
		self.play(Write(title))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho w = v3 $\\to$ v2 $\\to$ v5 $\\to$ v1',color=BLACK)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v3 $\\to$ v2 $\\to$ v5 $\\to$ v1',color=BLACK).move_to(TOP*0.8+LEFT*4)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [2,1,4,0]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00')
		self.play(ShowCreation(walk),run_time=10)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))

class EulerWalk(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+UP*2},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*4+UP*2},

			{'name': 'v4','pos': LEFT*4+DOWN*3},
			{'name': 'v5','pos': DOWN*3},
			{'name': 'v6','pos': RIGHT*4+DOWN*3},
		]
		ei = [
			(0,1),(1,2),(2,5),(0,3),(3,4),(4,5),(1,4)
		]
		
		title = TexText('Caminho Euleriano: todas as arestas são visitadas uma única vez',color=BLACK)
		sub = TexText('(vértices podem ser repetidos)',color=BLACK).move_to(DOWN)
		self.play(Write(title))
		self.play(FadeIn(sub, shift=TOP))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho\\\\ w = v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v2 $\\to$ v3 $\\to$ v6 $\\to$ v5',color=BLACK)
		self.play(FadeOut(sub, shift=BOTTOM),TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v2 $\\to$ v3 $\\to$ v6 $\\to$ v5',color=BLACK).move_to(TOP*0.8)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [1,0,3,4,1,2,5,4]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00')
		self.play(ShowCreation(walk),run_time=20)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))
class HamiltonianPath(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+UP*2},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*4+UP*2},

			{'name': 'v4','pos': LEFT*4+DOWN*3},
			{'name': 'v5','pos': DOWN*3},
			{'name': 'v6','pos': RIGHT*4+DOWN*3},
		]
		ei = [
			(0,1),(1,2),(2,5),(0,3),(3,4),(4,5)
		]
		
		title = TexText('Caminho Hamiltoniano: todo vértice é visitado uma única vez',color=BLACK)
		sub = TexText('(consequentemente, arestas também não são repetidas)',color=BLACK).move_to(DOWN)
		self.play(Write(title))
		self.play(FadeIn(sub, shift=TOP))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho\\\\ w = v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3',color=BLACK)
		self.play(FadeOut(sub, shift=BOTTOM),TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3',color=BLACK).move_to(TOP*0.8)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [1,0,3,4,5,2]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00')
		self.play(ShowCreation(walk),run_time=20)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))

class HamiltonianCicle(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+UP*2},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*4+UP*2},

			{'name': 'v4','pos': LEFT*4+DOWN*3},
			{'name': 'v5','pos': DOWN*3},
			{'name': 'v6','pos': RIGHT*4+DOWN*3},
		]
		ei = [
			(0,1),(1,2),(2,5),(0,3),(3,4),(4,5)
		]
		
		title = TexText('Ciclo Hamiltoniano: ciclo que visita todo vértice uma única vez',color=BLACK)
		sub = TexText('(exceto pelo último, que deve ser o inicial para fechar o ciclo)',color=BLACK).move_to(DOWN)
		self.play(Write(title), FadeIn(sub, shift=TOP))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho\\\\ w = v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3 $\\to$ v2',color=BLACK)
		self.play(TransformMatchingShapes(title,newtitle), FadeOut(sub, shift=BOTTOM))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3 $\\to$ v2',color=BLACK).move_to(TOP*0.8)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [1,0,3,4,5,2,1]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00',last_color='#0000FF')
		self.play(ShowCreation(walk),run_time=20)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))
class EulerianCicle(Scene):
	def construct(self):
		nd = [
			{'name': 'v1','pos': LEFT*4+UP*2},
			{'name': 'v2','pos': UP*2},
			{'name': 'v3','pos': RIGHT*4+UP*2},

			{'name': 'v4','pos': LEFT*4+DOWN*3},
			{'name': 'v5','pos': DOWN*3},
			{'name': 'v6','pos': RIGHT*4+DOWN*3},
		]
		ei = [
			(0,1),(1,2),(2,5),(0,3),(3,4),(4,5)
		]
		
		title = TexText('Ciclo Euleriano: ciclo que visita toda aresta uma única vez',color=BLACK)
		sub = TexText('(consequentemente, nenhum vértice é repetido, exceto o último)',color=BLACK).move_to(DOWN)
		self.play(Write(title), FadeIn(sub, shift=TOP))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho\\\\ w = v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3 $\\to$ v2',color=BLACK)
		self.play(TransformMatchingShapes(title,newtitle), FadeOut(sub, shift=BOTTOM))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3 $\\to$ v2',color=BLACK).move_to(TOP*0.8)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		walk_arr = [1,0,3,4,5,2,1]
		self.play(ShowCreation(g))
		walk = generate_graph_walk(walk_arr, vr,vcolors='#FF0000',ecolors='#00FF00',last_color='#0000FF')
		self.play(ShowCreation(walk),run_time=20)
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))
class GraphCicle(Scene):
	def construct(self):
		nd = [
			{'name':'v1','pos': LEFT*4+DOWN},
			{'name':'v2','pos': UP*2},
			{'name':'v3','pos': RIGHT*5+UP*3},
			{'name':'v4','pos': LEFT+DOWN*3},
			{'name':'v5','pos': RIGHT*2+DOWN*2},
		]

		ei = [
			(0,2),(0,3),(0,4),(2,0),(1,2),(1,3),(1,4),(4,1)
		]
		cicle = [0,3,1,4,0]
		title = TexText('Ciclo: caminho que se inicia em um vértice e termina no mesmo',color=BLACK)
		sub = TexText('(sem arestas repetidas)',color=BLACK).move_to(DOWN)
		self.play(Write(title), FadeIn(sub, shift=BOTTOM))
		self.wait(3)
		newtitle = TexText('Veja o exemplo do caminho\\\\ w = v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3',color=BLACK)
		self.play(FadeOut(sub, shift=BOTTOM),TransformMatchingShapes(title,newtitle))
		title=newtitle
		self.wait(3)
		newtitle = TexText('v2 $\\to$ v1 $\\to$ v4 $\\to$ v5 $\\to$ v6 $\\to$ v3',color=BLACK).move_to(TOP*0.8+LEFT*3)
		self.play(TransformMatchingShapes(title,newtitle))
		title=newtitle

		g, vr = graph_gen(nd,ei,data=True)
		self.play(ShowCreation(g))
		walk = generate_graph_walk(cicle, vr,vcolors='#FF0000',ecolors='#00FF00',last_color='#0000FF')
		self.play(ShowCreation(walk),run_time=20)
		warning = TexText('Vértice já explorado!',color='#0000FF').move_to(LEFT*4+DOWN*2)
		self.play(Write(warning))
		self.wait()
		self.wait(3)
		self.play(FadeOut(walk),FadeOut(title))
		self.wait()
		self.play(FadeOut(g))
class FindK5(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2+LEFT*3},
			{'name': 'b', 'pos': RIGHT+UP*3},
			{'name': 'c', 'pos': LEFT*1.2+UP*1.2},
			{'name': 'd', 'pos': RIGHT/2+UP*1.5},
			{'name': 'e', 'pos': RIGHT*3+UP/2},
			{'name': 'f', 'pos': RIGHT*2+DOWN*2},
			{'name': 'g', 'pos': LEFT+DOWN*3},
			{'name': 'h', 'pos': LEFT*2+DOWN},
			{'name': 'i', 'pos': ORIGIN},
		]
		ei = [(0,1),(0,7),(1,2),(1,3),(1,4),(1,5),(2,3),(2,6),(2,7),(2,8),(3,5),(3,7),(4,5),(5,6),(5,7),(5,8)]
		g = graph_gen(nd,ei,size=0.5)
		self.play(ShowCreation(g))
		self.wait(2)
		degrees = VGroup()
		titles = VGroup(
			TexText('$K_{5}$', color=BLACK).move_to(TOP*0.8),
			TexText('$K_{3,3}$', color=BLACK).move_to(TOP*0.8+RIGHT*2),
		)
		for i in range(len(nd)):
			degree = 0
			for j in range(len(ei)):
				if i == ei[j][0] or i == ei[j][1]:
					degree += 1
			text = TexText(str(degree), color='#FF0000').move_to(nd[i]['pos'])
			degrees.add(text)
		degrees.move_to(RIGHT/2)
		self.play(Write(titles), run_time=0.5)
		self.play(Write(degrees))
		self.wait(5)
		vcolors = {
			'b': RED,
			'c': RED,
			'd': RED,
			'f': RED,
			'h': RED,
		}
		self.play(Transform(g, graph_gen(nd,ei,vertex_colors=vcolors,size=0.5)))
		self.play(FadeOut(titles[1],shift=TOP))
		self.play(g.animate.scale(0.7).move_to(LEFT*4.7),degrees.animate.scale(0.7).move_to(LEFT*4.4))
		nd2 = [
			{'name': 'a', 'pos': ((((RIGHT*2+DOWN*2.5)+(RIGHT*3.5+UP*2))/2)+(RIGHT*2+DOWN*2.5))/2},
			{'name': 'b', 'pos': RIGHT*3.5+UP*2},
			{'name': 'c', 'pos': RIGHT},
			{'name': 'd', 'pos': RIGHT*5+DOWN*2.5},
			{'name': 'f', 'pos': RIGHT*6},
			{'name': 'h', 'pos': RIGHT*2+DOWN*2.5},
			{'name': 'i', 'pos': RIGHT*3.5},
			{'name': 'g', 'pos': RIGHT*2.8+UP},
		]
		ei2 = [
			(1,2),(1,4),(1,3),(2,3),(3,4),(3,5),(4,5),(5,2), (0,1),(0,5), (4,6),(2,6), (4,7),(2,7)
		]
		newgraph = g.copy()
		self.play(Transform(newgraph,graph_gen(nd2, ei2,size=0.5,vertex_colors=vcolors)),run_time=5)
		self.wait(3)
		self.play(FadeOut(g, shift=LEFT),FadeOut(degrees, shift=LEFT))
		self.play(newgraph.animate.move_to(ORIGIN))
		self.wait()
		
		nd2.pop(len(nd2)-1)
		ei2.pop(len(ei2)-1)
		ei2.pop(len(ei2)-1)
		self.play(Transform(newgraph,graph_gen(nd2, ei2,size=0.5,vertex_colors=vcolors).move_to(ORIGIN)),run_time=0.5)
		self.wait()
		nd2.pop(len(nd2)-1)
		ei2.pop(len(ei2)-1)
		ei2.pop(len(ei2)-1)
		ei2.append((2,4))
		self.play(Transform(newgraph,graph_gen(nd2, ei2,size=0.5,vertex_colors=vcolors).move_to(ORIGIN)),run_time=0.5)
		self.wait()
		nd2.pop(0)
		ei2.pop(len(ei2)-2)
		ei2.pop(len(ei2)-2)
		for i in range(len(ei2)):
			ei2[i] = (ei2[i][0]-1,ei2[i][1]-1)
		ei2.append((0,4))
		self.play(Transform(newgraph,graph_gen(nd2, ei2,size=0.5,vertex_colors=vcolors).move_to(ORIGIN)),run_time=0.5)
		self.wait(3)
		self.play(FadeOut(VGroup(titles[0], newgraph)))

class DepthFirstSearch(Scene):
	def construct(self):
		nd, ei = self.genrows(13)
		g, vr = graph_gen(nd,ei,size=0.5, data=True,color=BLUE,vertex_colors={'.': GREEN_SCREEN})
		title = TexText('Busca em profundidade', color=BLACK)
		self.play(Write(title))
		self.wait(3)
		self.play(FadeOut(title))
		self.play(ShowCreation(g),run_time=5)
		dfs_path = vr[45].dfs(vcolors='#FF0000',ecolors='#FF0000',found_color='#FF0000')
		self.play(ShowCreation(dfs_path), run_time=30)
		self.wait()
		self.play(FadeOut(dfs_path))
		self.play(FadeOut(g))
	def genrows(self,nodes):
		nd = []
		ei = []
		for row in range(-3,4):
			h = row
			w = 6
			for i in range(nodes):
				if i+13*(row+3) == 45:
					nd.append({'name': '.','pos': UP*h+LEFT*w})
				else:
					nd.append({'pos': UP*h+LEFT*w})
				if i < 12:
					ei.append((i+13*(row+3),i+1+13*(row+3)))
				if h == row+0.5:
					h -= 0.5
				else:
					h += 0.5
				w -= 1
		wid = 5
		for row in range(1,7):
			for node in range(1,12,2):
				ei.append((node+(row-1)*13,node+1+row*13))
				ei.append((node+(row-1)*13,node-1+row*13))
		for node in range(0,6):
			nd.append({'pos': DOWN*3.5+LEFT*wid})
			ei.append((7*13+node, node*2))
			ei.append((7*13+node, node*2+2))
			wid -= 2
		return nd,ei
class DFSCicleDetection(Scene):
	def construct(self):
		nd = [
			{'name': 'a', 'pos': UP*2+LEFT*3},
			{'name': 'b', 'pos': RIGHT+UP*3},
			{'name': 'c', 'pos': LEFT*1.2+UP*1.2},
			{'name': 'd', 'pos': RIGHT/2+UP*1.5},
			{'name': 'e', 'pos': RIGHT*3+UP/2},
			{'name': 'f', 'pos': RIGHT*2+DOWN*2},
			{'name': 'g', 'pos': LEFT+DOWN*3},
			{'name': 'h', 'pos': LEFT*2+DOWN},
			{'name': 'i', 'pos': ORIGIN},
		]
		ei = [(0,1),(0,7),(1,2),(1,3),(1,4),(1,5),(2,3),(2,6),(2,7),(2,8),(3,5),(3,7),(4,5),(5,6),(5,7),(5,8)]
		g, vr = graph_gen(nd,ei,size=0.75, data=True,color=ORANGE)
		self.play(ShowCreation(g))
		dfs_path = vr[1].dfs_cicle(vr[1],vcolors=GREEN_SCREEN,found_color='#FF0000')
		self.play(ShowCreation(dfs_path), run_time=20)
		self.wait()
		self.play(FadeOut(dfs_path))
		self.play(FadeOut(g))

class BreadthFirstSearch(Scene):
	def construct(self):
		nd, ei = self.genrows(13)
		g, vr = graph_gen(nd,ei,size=0.5, data=True,color=BLUE,vertex_colors={'.': GREEN_SCREEN})
		title = TexText('Busca em largura', color=BLACK)
		self.play(Write(title))
		self.wait(3)
		self.play(FadeOut(title))
		self.play(ShowCreation(g),run_time=5)
		bfs_path = vr[45].bfs(vcolors='#FF0000',ecolors='#FF0000',found_color='#FF0000')
		self.play(ShowCreation(bfs_path), run_time=60)
		self.wait()
		self.play(FadeOut(bfs_path))
		self.play(FadeOut(g))
	def genrows(self,nodes):
		nd = []
		ei = []
		for row in range(-3,4):
			h = row
			w = 6
			for i in range(nodes):
				if i+13*(row+3) == 45:
					nd.append({'name': '.','pos': UP*h+LEFT*w})
				else:
					nd.append({'pos': UP*h+LEFT*w})
				if i < 12:
					ei.append((i+13*(row+3),i+1+13*(row+3)))
				if h == row+0.5:
					h -= 0.5
				else:
					h += 0.5
				w -= 1
				count += 1
		wid = 5
		for row in range(1,7):
			for node in range(1,12,2):
				ei.append((node+(row-1)*13,node+1+row*13))
				ei.append((node+(row-1)*13,node-1+row*13))
		for node in range(0,6):
			nd.append({'pos': DOWN*3.5+LEFT*wid})
			ei.append((7*13+node, node*2))
			ei.append((7*13+node, node*2+2))
			wid -= 2
		return nd,ei

class BFSCovid(Scene):
	def construct(self):
		nd, ei = self.genrows(13)
		g, vr = graph_gen(nd,ei,size=0.5, data=True,color=BLUE,vertex_colors={'x': GREEN_SCREEN, '.': RED})
		title = TexText('Busca por infectado de COVID (em verde)', color=BLACK)
		self.play(Write(title))
		self.wait(3)
		self.play(FadeOut(title))
		self.play(ShowCreation(g),run_time=5)
		bfs_path,count = vr[45].bfs(vr[30],vcolors='#FF0000',ecolors='#FF0000',found_color='#0000FF', count_targets=True)
		self.play(ShowCreation(bfs_path), run_time=30)
		self.wait()
		self.play(FadeOut(bfs_path))
		self.play(FadeOut(g))
		found = TexText(str(count)+' infectados encontrados na rede',color=BLACK)
		self.play(Write(found))
		self.wait(3)
		self.play(FadeOut(found))
	def genrows(self,nodes):
		nd = []
		ei = []
		infected = [30,77,18,3,10,69,42]
		for row in range(-3,4):
			h = row
			w = 6
			for i in range(nodes):
				if i+13*(row+3) == 45:
					nd.append({'name': '.','pos': UP*h+LEFT*w})
				elif i+13*(row+3) in infected:
					nd.append({'name': 'x','pos': UP*h+LEFT*w})
				else:
					nd.append({'pos': UP*h+LEFT*w})
				if i < 12:
					ei.append((i+13*(row+3),i+1+13*(row+3)))
				if h == row+0.5:
					h -= 0.5
				else:
					h += 0.5
				w -= 1
		wid = 5
		for row in range(1,7):
			for node in range(1,12,2):
				ei.append((node+(row-1)*13,node+1+row*13))
				ei.append((node+(row-1)*13,node-1+row*13))
		for node in range(0,6):
			nd.append({'pos': DOWN*3.5+LEFT*wid})
			ei.append((7*13+node, node*2))
			ei.append((7*13+node, node*2+2))
			wid -= 2
		return nd,ei

class Sudoku(Scene):
	def construct(self):
		self.numbers_to_colors()
	def show_sudoku_example(self):
		grid = self.create_grid(9, 9, 0.5)
		grid_group = self.get_group_grid(grid)
		white_3x3_grid = self.create_grid(3, 3, 1.5)
		white_3x3_grid_group = self.get_group_grid(white_3x3_grid)

		grid_group.set_color(GRAY)
		grid_group.move_to(ORIGIN)
		white_3x3_grid_group.set_color(WHITE)
		white_3x3_grid_group.move_to(ORIGIN)
		self.play(
			FadeIn(grid_group),
			FadeIn(white_3x3_grid_group)
		)

		example_quiz = self.get_sudoku_quiz()
		number_objects = self.display_numbers_on_grid(example_quiz, grid)
		number_objects_group = []
		for row in number_objects:
			for obj in row:
				if obj != 0:
					number_objects_group.append(obj)
		number_objects_group = VGroup(*number_objects_group)
		self.wait(16)

		self.explain_rules()

		shifted_grid = grid_group.copy()
		shifted_3x3 = white_3x3_grid_group.copy()
		shifted_numbers = number_objects_group.copy()

		shifted_grid.shift(LEFT * 3)
		shifted_numbers.shift(LEFT * 3)
		shifted_3x3.shift(LEFT * 3)
		self.play(
			Transform(grid_group, shifted_grid),
			Transform(number_objects_group, shifted_numbers),
			Transform(white_3x3_grid_group, shifted_3x3)
		)
		self.wait()

		self.play(
			Indicate(VGroup(*grid_group[3:6][3:6]))
		)

		sub_grid_3x3 = self.create_grid(3, 3, 0.5)
		sub_grid_3x3_group = self.get_group_grid(sub_grid_3x3)
		sub_grid_3x3_group.move_to(grid_group.get_center())

		for row in sub_grid_3x3:
			for square in row:
				square.set_stroke(color=SKY_BLUE)

		color_map = {
		0: SKY_BLUE, 
		1: PINK, 
		2: BRIGHT_RED, 
		3: TEAL_E, 
		4: GOLD,
		5: ORANGE,
		6: YELLOW,
		7: WHITE,
		8: GREEN_SCREEN,
		9: BLUE
		}

		inverted_map = {}
		for key, val in color_map.items():
			inverted_map[val] = key

		self.wait(6)

		self.map_numbers_to_color(number_objects, color_map)
		self.wait(3)
		self.play(
			FadeIn(sub_grid_3x3_group)
		)
		self.wait(2)

		graph, edge_dict, data = self.make_graph_from_grid(example_quiz, [(3, 3), (6, 6)])
		nodes, edges = self.make_3x3_graph_mobject(graph, edge_dict, color_map, data, show_data=False)
		entire_graph = VGroup(nodes, edges)
		entire_graph.shift(RIGHT * 3)
		self.play(
			TransformFromCopy(sub_grid_3x3_group, entire_graph),
			run_time=2
		)

		self.wait(7)


		graph_1_1 = entire_graph.copy()
		graph_1_1.scale(0.33)


		self.play(
			ReplacementTransform(entire_graph, graph_1_1)
		)

		self.wait()

		other_grids = []
		for i in range(8):
			blue_grid = self.create_grid(3, 3, 0.5)
			blue_grid_group = self.get_group_grid(blue_grid)
			blue_grid_group.move_to(grid_group.get_center())

			for row in blue_grid:
				for square in row:
					square.set_stroke(color=SKY_BLUE)
			other_grids.append(blue_grid_group)

		white_grid = self.create_grid(1, 1, 1.5)
		white_grid_group = self.get_group_grid(white_grid)
		white_grid_group.move_to(grid_group.get_center())

		other_grids[0].shift(LEFT * 1.5 + UP * 1.5)
		other_grids[1].shift(UP * 1.5)
		other_grids[2].shift(RIGHT * 1.5 + UP * 1.5)
		other_grids[3].shift(LEFT * 1.5)
		other_grids[4].shift(RIGHT * 1.5)
		other_grids[5].shift(LEFT * 1.5 + DOWN * 1.5)
		other_grids[6].shift(DOWN * 1.5)
		other_grids[7].shift(RIGHT * 1.5 + DOWN * 1.5)

		other_grids.append(white_grid_group)

		other_grid_entire_group = VGroup(*other_grids)
		self.play(
			FadeOut(sub_grid_3x3_group)
		)
		self.play(
			FadeIn(other_grid_entire_group),
			run_time=2
		)

		all_graphs, all_graphs_mobjects = self.make_remaining_graphs(example_quiz, color_map)
		all_graphs[4], all_graphs_mobjects[4] = graph, entire_graph

		all_graphs_mobjects[0].shift(UP * 1.8 + LEFT * 1.8)
		all_graphs_mobjects[1].shift(UP * 1.8)
		all_graphs_mobjects[2].shift(UP * 1.8 + RIGHT * 1.8)
		all_graphs_mobjects[3].shift(LEFT * 1.8)
		all_graphs_mobjects[5].shift(RIGHT * 1.8)
		all_graphs_mobjects[6].shift(DOWN * 1.8 + LEFT * 1.8)
		all_graphs_mobjects[7].shift(DOWN * 1.8)
		all_graphs_mobjects[8].shift(DOWN * 1.8 + RIGHT * 1.8)

		remaining_graph_mobject = VGroup(all_graphs_mobjects[:4], all_graphs_mobjects[5:])
		remaining_grid = VGroup(other_grid_entire_group[:8])
		self.play(
			FadeIn(remaining_graph_mobject),
			run_time=2
		)

		self.wait()
		self.play(
			FadeOut(other_grid_entire_group)
		)

		self.wait(5)
		self.make_row_and_column_connections(all_graphs_mobjects)
		self.wait(10)

		example_solution = self.get_sudoku_solution()

		self.simulate_9x9(all_graphs_mobjects, grid, example_quiz, example_solution, color_map)

		self.wait(5)
		
		# self.display_numbers_on_grid(example_solution, grid)
		# self.wait()
		def simulate_9x9(self, all_graphs_mobjects, grid, number_objects, solution, color_map):
		quadrants = self.get_quadrants_list()
		all_transforms = []
		for i in range(9):
			transforms = self.simulate_3x3(all_graphs_mobjects[i][0], grid, number_objects, quadrants[i], solution, color_map)
			all_transforms.append(transforms)

		for i in range(len(all_transforms[0])):
			step_transforms = sum([transforms[i] for transforms in all_transforms], [])
			self.play(
				*step_transforms,
				run_time=0.1
			)
		self.wait()

	def simulate_3x3(self, nodes, grid, number_objects, quadrant, solution, color_map):
		relevant_data = self.get_relevant_data(number_objects, quadrant)
		solution = self.get_relevant_data(solution, quadrant)
		relevant_positions = self.get_relevant_positions(grid, quadrant)
		positions = []
		present_numbers = []
		graph_indices = []
		solution_map = {}
		for i in range(len(relevant_data)):
			for j in range(len(relevant_data[0])):
				if relevant_data[i][j] == 0:
					positions.append(relevant_positions[i][j])
					graph_indices.append(j * len(relevant_data) + i)
					solution_map[(relevant_positions[i][j][0], relevant_positions[i][j][1])] = solution[i][j]
				else:
					present_numbers.append(relevant_data[i][j])
		missing_numbers = set(list(range(1, 10))) - set(present_numbers)
		missing_numbers = list(missing_numbers)
		missing_objects = []
		missing_graph_objects = []
		num_to_text = {}
		
		for i, num in enumerate(missing_numbers):
			text = TextMobject(str(num))
			text.scale(0.6)
			text.move_to(positions[i])
			text.set_color(color_map[num])
			missing_objects.append((text, num))
			num_to_text[num] = text.copy()
			index = graph_indices[i]
			new_node = nodes[index].copy()
			new_node.set_color(color_map[num])
			missing_graph_objects.append(new_node)

		replacements = [ReplacementTransform(nodes[graph_indices[i]], 
			missing_graph_objects[i]) for i in range(len(missing_graph_objects))]
		transforms = []
		transforms.append([FadeIn(obj[0]) for obj in missing_objects] + replacements)
		
		iters = 50
		for _ in range(iters):
			new_missing_objects = [(obj[0].copy(), obj[1]) for obj in missing_objects]
			new_missing_graph_objects = [obj.copy() for obj in missing_graph_objects]
			random.shuffle(new_missing_objects)
			for i in range(len(new_missing_objects)):
				new_missing_objects[i][0].move_to(positions[i])
				num = new_missing_objects[i][1]
				new_missing_graph_objects[i].set_color(color_map[num])

			transform = [ReplacementTransform(missing_objects[i][0], 
				new_missing_objects[i][0]) for i in range(len(missing_objects))]
			graph_transforms = [ReplacementTransform(missing_graph_objects[i], 
				new_missing_graph_objects[i]) for i in range(len(missing_graph_objects))]
			transforms.append(transform + graph_transforms)
			for i in range(len(missing_objects)):
				missing_objects[i] = new_missing_objects[i]
				missing_graph_objects[i] = new_missing_graph_objects[i]

		solution_transforms = []
		solution_object_sequence = []
		solution_object_colors = [obj.copy() for obj in missing_graph_objects]
		for i in range(len(missing_objects)):
			num = solution_map[(positions[i][0], positions[i][1])]
			text = num_to_text[num].copy()
			text.move_to(positions[i])
			solution_object_sequence.append((text, num))
			solution_object_colors[i].set_color(color_map[num])

		# self.play(*[FadeOut(missing_objects[i][0]) for i in range(len(missing_objects))])
		# self.wait()
		# self.play(*[FadeIn(solution_object_sequence[i][0]) for i in range(len(missing_objects))])

		solution_transforms = [ReplacementTransform(missing_objects[i][0], 
			solution_object_sequence[i][0]) for i in range(len(missing_objects))]
		solution_graph_transforms = [ReplacementTransform(missing_graph_objects[i], 
			solution_object_colors[i]) for i in range(len(missing_graph_objects))]

		transforms.append(solution_transforms + solution_graph_transforms)
		return transforms
	def make_row_and_column_connections(self, all_graphs_mobjects, scale_factor=0.33):
		all_graphs_nodes = [obj[0] for obj in all_graphs_mobjects]
		row_edges = []
		row_edges.extend(self.connect_row(all_graphs_nodes[0], all_graphs_nodes[1]))
		row_edges.extend(self.connect_row(all_graphs_nodes[1], all_graphs_nodes[2]))
		row_edges.extend(self.connect_row(all_graphs_nodes[3], all_graphs_nodes[4]))
		row_edges.extend(self.connect_row(all_graphs_nodes[4], all_graphs_nodes[5]))
		row_edges.extend(self.connect_row(all_graphs_nodes[6], all_graphs_nodes[7]))
		row_edges.extend(self.connect_row(all_graphs_nodes[7], all_graphs_nodes[8]))

		self.play(
			*[ShowCreation(edge) for edge in row_edges]
		)

		column_edges = []
		column_edges.extend(self.connect_column(all_graphs_nodes[0], all_graphs_nodes[3]))
		column_edges.extend(self.connect_column(all_graphs_nodes[1], all_graphs_nodes[4]))
		column_edges.extend(self.connect_column(all_graphs_nodes[2], all_graphs_nodes[5]))
		column_edges.extend(self.connect_column(all_graphs_nodes[3], all_graphs_nodes[6]))
		column_edges.extend(self.connect_column(all_graphs_nodes[4], all_graphs_nodes[7]))
		column_edges.extend(self.connect_column(all_graphs_nodes[5], all_graphs_nodes[8]))

		self.wait(6)
		self.play(
			*[ShowCreation(edge) for edge in column_edges]
		)

	def connect_row(self, graph1, graph2, scale_factor=0.33, edge_color=GRAY):
		row_edges = []
		nodes_graph_1 = [graph1[i] for i in range(6, 9)]
		nodes_graph_2 = [graph2[i] for i in range(3)]
		for node1, node2 in zip(nodes_graph_1, nodes_graph_2):
			start_point = node1.get_center() + RIGHT * 0.4 * scale_factor
			end_point = node2.get_center() + LEFT * 0.4 * scale_factor
			edge = Line(start_point, end_point)
			edge.set_stroke(width=7)
			edge.set_color(color=edge_color)
			row_edges.append(edge)
		return row_edges

	def connect_column(self, graph1, graph2, scale_factor=0.33, edge_color=GRAY):
		column_edges = []
		nodes_graph_1 = [graph1[i] for i in range(2, 9, 3)]
		nodes_graph_2 = [graph2[i] for i in range(0, 9, 3)]
		for node1, node2 in zip(nodes_graph_1, nodes_graph_2):
			start_point = node1.get_center() + DOWN * 0.4 * scale_factor
			end_point = node2.get_center() + UP * 0.4 * scale_factor
			edge = Line(start_point, end_point)
			edge.set_stroke(width=7)
			edge.set_color(color=edge_color)
			column_edges.append(edge)
		return column_edges

	def get_quadrants_list(self):
		quadrants = []
		for i in range(3):
			for j in range(3):
				quadrants.append([(i * 3, j * 3), (i * 3 + 3, j * 3 + 3)])
		return quadrants

	def make_remaining_graphs(self, example_quiz, color_map):
		all_graphs = [0] * 9
		all_graphs_mobjects = [0] * 9
		
		quadrants = self.get_quadrants_list()
		# print(quadrants)
		for i, quadrant in enumerate(quadrants):
			if i == 4:
				continue
			graph, edge_dict, data = self.make_graph_from_grid(example_quiz, quadrant)
			nodes, edges = self.make_3x3_graph_mobject(graph, edge_dict, color_map, data, show_data=False)
			entire_graph = VGroup(nodes, edges)
			entire_graph.shift(RIGHT * 3)
			entire_graph.scale(0.33)
			all_graphs[i] = graph
			all_graphs_mobjects[i] = entire_graph
		return all_graphs, all_graphs_mobjects

	def map_numbers_to_color(self, number_objects, color_map):
		for row in number_objects:
			for obj in row:
				if obj != 0:
					obj.set_color(color_map[int(obj.tex_string)])
	
	def get_relevant_positions(self, grid, quadrant):
		relevant_data = [[0] * 3 for _ in range(3)]
		# print(quadrant)
		for i in range(quadrant[0][0], quadrant[1][0]):
			for j in range(quadrant[0][1], quadrant[1][1]):
				relevant_data[i - quadrant[0][0]][j - quadrant[0][1]] = grid[i][j].get_center()
		return relevant_data

	def get_relevant_data(self, number_objects, quadrant):
		relevant_data = [[0] * 3 for _ in range(3)]
		# print(quadrant)
		for i in range(quadrant[0][0], quadrant[1][0]):
			for j in range(quadrant[0][1], quadrant[1][1]):
				relevant_data[i - quadrant[0][0]][j - quadrant[0][1]] = number_objects[i][j]
		return relevant_data

	def make_graph_from_grid(self, number_objects, quadrant):
		# quadrant = [(i, j), (k, l)]
		relevant_data = self.get_relevant_data(number_objects, quadrant)
		return self.make_3x3_graph(relevant_data)

	def make_3x3_graph(self, data):
		radius, scale = 0.4, 0.8
		graph = [[0] * 3 for _ in range(3)]
		edge_dict = {}
		for i in range(3):
			for j in range(3):
				entry = data[j][i]
				graph[i][j] = GraphNode(entry, position=RIGHT * (i - 1) * 2 + DOWN * (j - 1) * 2, radius=radius, scale=scale)

		for i in range(2):
			edge_dict[((i, 0), (i + 1, 0))]	= graph[i][0].connect(graph[i + 1][0])
			edge_dict[((i, 1), (i + 1, 1))] = graph[i][1].connect(graph[i + 1][1])
			edge_dict[((i, 2), (i + 1, 2))] = graph[i][2].connect(graph[i + 1][2])

		for i in range(2):
			edge_dict[((0, i), (0, i + 1))]	= graph[0][i].connect(graph[0][i + 1])
			edge_dict[((1, i), (1, i + 1))] = graph[1][i].connect(graph[1][i + 1])
			edge_dict[((2, i), (2, i + 1))] = graph[2][i].connect(graph[2][i + 1])

		for i in range(2):
			for j in range(2):
				edge_dict[((i, j), (i + 1, j + 1))]	= graph[i][j].connect(graph[i + 1][j + 1])
		
		for i in range(1, 3):
			for j in range(2):
				edge_dict[((i, j), (i - 1, j + 1))]	= graph[i][j].connect(graph[i - 1][j + 1])
		
		edge_dict[((0, 1), (2, 0))]	= graph[0][1].connect(graph[2][0])
		edge_dict[((0, 1), (2, 2))] = graph[0][1].connect(graph[2][2])
		edge_dict[((0, 0), (2, 1))] = graph[0][0].connect(graph[2][1])
		edge_dict[((0, 2), (2, 1))] = graph[0][2].connect(graph[2][1])
		edge_dict[((0, 0), (1, 2))] = graph[0][0].connect(graph[1][2])
		edge_dict[((2, 0), (1, 2))] = graph[2][0].connect(graph[1][2])
		edge_dict[((1, 0), (0, 2))]	= graph[1][0].connect(graph[2][0])
		edge_dict[((1, 0), (2, 2))]	= graph[1][0].connect(graph[2][2])
		# edge_dict[((1, 0), (2, 1))] = graph[1][i].connect(graph[1][i + 1])
		# edge_dict[((2, i), (2, i + 1))] = graph[2][i].connect(graph[2][i + 1])

		return graph, edge_dict, data

	def make_3x3_graph_mobject(self, graph, edge_dict, color_map, data, scale_factor=1, edge_color=GRAY, show_data=True):
		nodes = []
		edges = []
		for i in range(len(graph)):
			for j in range(len(graph[0])):
				node = graph[i][j]
				num = data[j][i]
				color = color_map[num]
				node.circle.set_fill(color=color, opacity=0.5)
				node.circle.set_stroke(color=color)
				if num == 0 or not show_data:
					nodes.append(node.circle)
				else:
					node.data.set_color(BLACK)
					nodes.append(VGroup(node.circle, node.data))

		for edge in edge_dict.values():
			edge.set_stroke(width=7*scale_factor)
			edge.set_color(color=edge_color)
			edges.append(edge)
		return VGroup(*nodes), VGroup(*edges)

	def get_sudoku_quiz(self):
		quiz_string = '005960070400031020106705008900408060002013800058000000200096007007020615003000004'
		return self.parse_string_to_2d_list(quiz_string)

	def get_sudoku_solution(self):
		solution_string = '385962471479831526126745938931458762742613859658279143214596387897324615563187294'
		return self.parse_string_to_2d_list(solution_string)

	def parse_string_to_2d_list(self, string):
		quiz = []
		for i in range(9):
			row = string[i * 9: (i + 1) * 9]
			quiz.append([int(c) for c in row])
		return quiz

	def display_numbers_on_grid(self, sudoku_list, grid):
		number_objects = [[0] * 9 for _ in range(9)]
		for i in range(9):
			for j in range(9):
				num = sudoku_list[i][j]
				if num != 0:
					text = TextMobject(str(num))
					text.scale(0.6)
					position = grid[i][j].get_center()
					text.move_to(position)
					# print(i, j, 'Write')
					number_objects[i][j] = text
		self.render_numbers(number_objects)
		return number_objects

	def render_numbers(self, number_objects):
		animations = []
		for row in number_objects:
			for obj in row:
				if obj != 0:
					animations.append(FadeIn(obj))
		self.play(
			*animations
		)

	def explain_rules(self, color=GREEN_SCREEN):
		sub_grid_3x3 = self.create_grid(3, 3, 0.5)
		sub_grid_3x3_group = self.get_group_grid(sub_grid_3x3)
		sub_grid_3x3_group.move_to(ORIGIN)

		for row in sub_grid_3x3:
			for square in row:
				square.set_stroke(color=color)
				square.set_fill(color=color, opacity=0.2)
		self.play(
			GrowFromCenter(sub_grid_3x3_group)
		)
		self.wait(3)

		sub_grid_1x9 = self.create_grid(1, 9, 0.5)
		sub_grid_1x9_group = self.get_group_grid(sub_grid_1x9)
		sub_grid_1x9_group.move_to(ORIGIN)
		for row in sub_grid_1x9:
			for square in row:
				square.set_stroke(color=color)
				square.set_fill(color=color, opacity=0.2)

		self.play(
			ReplacementTransform(sub_grid_3x3_group, sub_grid_1x9_group),
			run_time=2
		)
		self.wait(3)

		sub_grid_9x1 = self.create_grid(9, 1, 0.5)
		sub_grid_9x1_group = self.get_group_grid(sub_grid_9x1)
		sub_grid_9x1_group.move_to(ORIGIN)
		for row in sub_grid_9x1:
			for square in row:
				square.set_stroke(color=color)
				square.set_fill(color=color, opacity=0.2)

		self.play(
			ReplacementTransform(sub_grid_1x9_group, sub_grid_9x1_group),
			run_time=2
		)
		self.wait(3)

		self.play(
			FadeOut(sub_grid_9x1_group)
		)

	def create_grid(self, rows, columns, square_length):
		left_corner = Square(side_length=square_length)
		grid = []
		first_row = [left_corner]
		for i in range(columns - 1):
			square = Square(side_length=square_length)
			square.next_to(first_row[i], RIGHT, buff=0)
			first_row.append(square)
		grid.append(first_row)
		for i in range(rows - 1):
			prev_row = grid[i]
			new_row = []
			for square in prev_row:
				square_below = Square(side_length=square_length)
				square_below.next_to(square, DOWN, buff=0)
				new_row.append(square_below)
			grid.append(new_row)

		return grid

	def get_group_grid(self, grid):
		squares = []
		for row in grid:
			for square in row:
				squares.append(square)
		return VGroup(*squares)
		
	def highlight_road_edge(self, dots, u, v, color=GREEN_SCREEN):
		assert u < v
		center_u, center_v = dots[u].get_center(), dots[v].get_center()
		unit_vector = Line(center_u, center_v).get_unit_vector()
		start = center_u + unit_vector * dots[u].radius
		end = center_v - unit_vector * dots[v].radius
		edge = Line(start, end)
		edge.set_stroke(color=color, width=14)
		return edge

	def find_intersection_points(self, road_network):
		points = []
		for i in range(3):
			for j in range(3, 6):
				if j == 3:
					point = self.find_intersection_two_rects(road_network[j], 
						road_network[i], slant=True)
				else:
					point = self.find_intersection_two_rects(road_network[j], 
						road_network[i], slant=False)
				points.append(point)

		return points

	def find_intersection_two_rects(self, vert, horz, slant=False):
		if slant:
			p1_vert = self.get_bottom_left_corner(vert)
			p2_vert = self.get_top_left_corner(vert)

			p3_vert = self.get_bottom_right_corner(vert)
			p4_vert = self.get_top_right_corner(vert)
			A = (p1_vert + p2_vert) / 2 
			B = (p3_vert + p4_vert) / 2
		else:
			p1_vert = self.get_top_right_corner(vert)
			p2_vert = self.get_top_left_corner(vert)

			p3_vert = self.get_bottom_right_corner(vert)
			p4_vert = self.get_bottom_left_corner(vert)
			A = (p1_vert + p2_vert) / 2 
			B = (p3_vert + p4_vert) / 2


		p1_horz = self.get_bottom_left_corner(horz)
		p2_horz = self.get_top_left_corner(horz)

		p3_horz = self.get_bottom_right_corner(horz)
		p4_horz = self.get_top_right_corner(horz)
		C = (p1_horz + p2_horz) / 2 
		D = (p3_horz + p4_horz) / 2

		line1 = (A, B)
		line2 = (C, D)
		return line_intersection(line1, line2)

	def get_top_left_corner(self, rect):
		corners = rect.get_anchors()
		return corners[0]

	def get_top_right_corner(self, rect):
		corners = rect.get_anchors()
		return corners[1]

	def get_bottom_right_corner(self, rect):
		corners = rect.get_anchors()
		return corners[2]

	def get_bottom_left_corner(self, rect):
		corners = rect.get_anchors()
		return corners[3]

	def make_equivalent_graph(self):
		graph = []
		edges = {}

		radius, scale = 0.4, 0.8
		SHIFT = RIGHT * 2.5
		node_0 = GraphNode('0', position=RIGHT * 0.5 + UP * 2, radius=radius, scale=scale)
		node_1 = GraphNode('1', position=RIGHT * 3 + UP * 2, radius=radius, scale=scale)
		node_2 = GraphNode('2', position=RIGHT * 5 + UP * 2, radius=radius, scale=scale)
		node_3 = GraphNode('3', position=RIGHT * 1, radius=radius, scale=scale)
		node_4 = GraphNode('4', position=RIGHT * 3, radius=radius, scale=scale)
		node_5 = GraphNode('5', position=RIGHT * 5, radius=radius, scale=scale)
		node_6 = GraphNode('6', position=RIGHT * 1.5 + DOWN * 2, radius=radius, scale=scale)
		node_7 = GraphNode('7', position=RIGHT * 3 + DOWN * 2, radius=radius, scale=scale)
		node_8 = GraphNode('8', position=RIGHT * 5 + DOWN * 2, radius=radius, scale=scale)


		edges[(0, 1)] = node_0.connect(node_1)
		edges[(0, 3)] = node_0.connect(node_3)
		
		edges[(1, 2)] = node_1.connect(node_2)
		edges[(1, 4)] = node_1.connect(node_4)

		edges[(2, 5)] = node_2.connect(node_5)

		edges[(3, 4)] = node_3.connect(node_4)
		edges[(3, 6)] = node_3.connect(node_6)
		
		edges[(4, 5)] = node_4.connect(node_5)
		edges[(4, 7)] = node_4.connect(node_7)

		edges[(5, 8)] = node_5.connect(node_8)

		edges[(6, 7)] = node_6.connect(node_7)

		edges[(7, 8)] = node_7.connect(node_8)

		graph.append(node_0)
		graph.append(node_1)
		graph.append(node_2)
		graph.append(node_3)
		graph.append(node_4)
		graph.append(node_5)
		graph.append(node_6)
		graph.append(node_7)
		graph.append(node_8)

		return graph, edges
